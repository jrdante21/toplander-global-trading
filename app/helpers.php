<?php

if (!function_exists('app_details')) {
    function app_details()
    {
        $details = file_get_contents(resource_path('js/constants/appDetails.json'));
        $details = json_decode($details, true);
        
        $packages = file_get_contents(resource_path('js/constants/packageTypes.json'));
        $packages = json_decode($packages, true);

        $details['packages'] = $packages;
        return $details;
    }
}

if (!function_exists('product_categories')) {
    function product_categories()
    {
        $file = file_get_contents(resource_path('js/constants/productsCategories.json'));
        return json_decode($file, true);
    }
}