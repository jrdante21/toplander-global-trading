<?php

namespace App\Utilities;
use Illuminate\Support\Str;
use App\Models\MembersNetwork;
use App\Models\MembersNetworkPairings;

class ProfitBinary
{
    private $formula = [
        'pairing_1' =>  528, // 3K Binary Pairing
        'pairing_2' =>  880, // 7K Binary Pairing
        'directs_1' =>  264, // 3K Binary Sponsoring
        'directs_2' =>  528, // 7K Binary Sponsoring
        'mill' =>  500, // 2K Millennium Sponsoring
        'millLower' =>  50, // 2K Millennium Lower Sponsoring
    ];

    private $date;
    private $cycle;
    private $packageType;

    public function updateUplines($downlineID, $datetime='')
    {   
        // Downline Data
        $downline = MembersNetwork::find($downlineID);
        if (empty($downline)) return false;

        $this->packageType = $downline->type;

        if (empty($datetime)) {
            $this->date = date('Y-m-d', strtotime($downline->created_at));
            $this->cycle = $downline->cycle;
        } else {
            $this->date = date('Y-m-d', strtotime($datetime));
            $this->cycle = (date('Hi', strtotime($datetime)) <= 1200) ? 1 : 2;
        }

        // Downline nodes
        $nodes = [explode('.', $downline->b_nodes), explode('.', $downline->s_nodes)];
        $nodes = collect($nodes)->collapse()->filter()->unique()->values();

        return $this->__updateMembers($nodes, $downline->sponsored_by);
    }

    public function refreshMembers($ids, $datetime, $type)
    {
        $this->packageType = $type;
        $this->date = date('Y-m-d', strtotime($datetime));
        $this->cycle = (date('Hi', strtotime($datetime)) <= 1200) ? 1 : 2;
        return $this->__updateMembers($ids, 0, true); 
    }

    private function __updateMembers($ids, $sponsorID=0, $all=false)
    {
        $type = max($this->packageType, 1);
        $date = $this->date;
        $cycle = $this->cycle;

        // Last date & cycle if 3K & 7K binary
        if ($this->packageType >= 1) {
            $date = ($this->cycle <= 1) ? date('Y-m-d', strtotime('-1 day', strtotime($this->date))) : $this->date;
            $cycle = ($this->cycle <= 1) ? 2 : 1;
        }
        
        $query = MembersNetwork::whereIn('id', $ids)
        ->where('type', '>=', $this->packageType)
        ->latestPairings($date, $cycle)
        ->maintenance($this->date)
        ->get();

        $data = [];
        foreach ($query as $q) {
            if (!$all) {
                $latestLft = 'latest_lft_'.$type;
                $latestRgt = 'latest_rgt_'.$type;
                if (!empty($q->$latestLft + $q->$latestRgt) || $q->status <= 2 || $sponsorID == $q->id) $data[] = $this->__update($q);
            } else {
                $data[] = $this->__update($q);
            }
        }

        return $data;
    }

    private function __update($memberData)
    {
        // Get the current entries
        $current = $this->__currentEntries([
            'id' => $memberData->id,
            'lft' => $memberData->lft_leg,
            'rgt' => $memberData->rgt_leg,
        ]);
        
        // Sponsoring
        $spon = $this->__computeSponsoring($memberData, $current);

        // Compute pairings when 3K & 7K Binary
        if ($this->packageType >= 1) {
            $pair = $this->__computePairings($memberData, $current);
        } else {
            $pair['lft'] = (!empty($memberData->latest_lft_1)) ? $memberData->latest_lft_1 : 0;
            $pair['rgt'] = (!empty($memberData->latest_rgt_1)) ? $memberData->latest_rgt_1 : 0;
        }
        
        // Add to update
        $computed = collect([$pair, $spon])->collapse()->all();
        $rowMatch = [
            'members_network_id' => $memberData->id,
            'cycle' => $this->cycle,
            'date' => $this->date,
            'type' => max($this->packageType, 1),
        ];

        // Update
        MembersNetworkPairings::updateOrInsert($rowMatch, $computed);
        return [$memberData, $current];
    }

    private function __computePairings($memberData, $current)
    {
        $status = $memberData->status;
        $type = max($this->packageType, 1);
        $data['paired'] = 0;
        $data['fifth'] = 0;
        $data['b_pts'] = 0;

        // Current Legs
        $currentLft = count($current['lft'.$type]);
        $currentRgt = count($current['rgt'.$type]);

        // Latest Legs
        $latestLft = $memberData['latest_lft_'.$type];
        $latestRgt = $memberData['latest_rgt_'.$type];

        // Compute pairings if active
        if ($status <= 2) {
            $lft = ($currentLft + $latestLft); // Left
            $rgt = ($currentRgt + $latestRgt); // Right
            $paired = (min($lft, $rgt) - min($latestLft, $latestRgt)); // Paired

            $fifth = floor((
                ($memberData['latest_paired_'.$type] % 5) + // Latest Paired
                $paired // Computed Paired
            ) / 5); // Fifth Pair
            
            // Computed paired minus fifth pair but 4 pairs only ;<
            if ($paired >= 5) {
                $paired = 5;
                $fifth = 1;
                $lft = 0;
                $rgt = 0;
            }
            
            // Pairing Points
            $b_pts = $paired - $fifth;
            $b_pts = ($b_pts * $this->formula['pairing_'.$type]); // Multiply by pairing profit

            $data['lft'] = $lft;
            $data['rgt'] = $rgt;
            $data['paired'] = $paired;
            $data['fifth'] = $fifth;
            $data['b_pts'] = $b_pts;

        // Less 1 on greater number if not active ;<
        } else {
            $countToMinus = ($currentLft + $currentRgt);
            if ($latestLft > $latestRgt) {
                $data['lft'] = max(($latestLft - $countToMinus), 0);
                $data['rgt'] = $latestRgt;
            } else {
                $data['lft'] = $latestLft;
                $data['rgt'] = max(($latestRgt - $countToMinus), 0);
            }
        }
        return $data;
    }

    private function __computeSponsoring($memberData, $current)
    {
        $status = $memberData->status;
        $type = max($this->packageType, 1);

        // 3K & 7K Binary
        $data['directs'] = count($current['direct'.$type]); // Directs
        $data['s_pts'] = ($data['directs'] * $this->formula['directs_'.$type]); // Points

        // 2K Millennium
        if ($type <= 1) {
            $millCount = count($current['mill']);
            $data['u_pts'] = ($millCount * $this->formula['mill']);
            $data['u_pts'] += ($status <= 2) ? (count($current['millLower']) * $this->formula['millLower']) : 0; // Lower millenniums
            $data['directs'] += $millCount;
        } else {
            $data['u_pts'] = 0;
        }

        return $data;
    }

    private function __currentEntries($memberData) // ['id', 'lft', 'rgt']
    {
        $date = $this->date;
        $cycle = $this->cycle;

        if (empty($memberData['id'])) return null;

        $mainID = $memberData['id'];
        $mainLFT = (empty($memberData['lft'])) ? 0 : $memberData['lft'];
        $mainRGT = (empty($memberData['rgt'])) ? 0 : $memberData['rgt'];
        
        $dc = $date.'-0'.$cycle;
        $dcWhere = [];
        foreach (['members_network', 'members_upgrade'] as $d) {
            $dcWhere[$d] = "CONCAT(DATE($d.created_at), '-0', (CASE WHEN (DATE_FORMAT($d.created_at, '%H%i') <= 1200) THEN 1 ELSE 2 END))";
        }

        $query = MembersNetwork::selectRaw("
            CONCAT(fname, ' ', lname) as name,
            members_network.username,
            members_network.type,
            {$dcWhere['members_network']} as dc_network,
            {$dcWhere['members_upgrade']} as dc_upgrade,
            (CASE WHEN (LOCATE(?, b_nodes) >= 1 OR ? = members_network.id) THEN 1 ELSE 0 END) as is_lft,
            (CASE WHEN (LOCATE(?, b_nodes) >= 1 OR ? = members_network.id) THEN 1 ELSE 0 END) as is_rgt,
            (CASE WHEN (? = sponsored_by) THEN 1 ELSE 0 END) as is_direct,
            LOCATE(CONCAT('.', sponsored_by, '.', ?, '.'), s_nodes) as is_lower,
            members_network.created_at
        ", [
            ".{$mainLFT}.", $mainLFT,
            ".{$mainRGT}.", $mainRGT,
            $mainID, 
            $mainID
        ])
        ->leftJoin('members', 'members_network.members_id', '=', 'members.id')
        ->leftJoin('members_network_upgrades as members_upgrade', 'members_network.id', '=', 'members_upgrade.members_network_id')
        ->whereRaw("(b_nodes LIKE ? OR s_nodes LIKE ?)", ['%.'.$mainID.'.%', '%.'.$mainID.'.%'])
        ->whereRaw("({$dcWhere['members_network']} = ? OR {$dcWhere['members_upgrade']} = ?)", [$dc, $dc])
        ->get();

        $no_upgrade = collect($query)->where('dc_upgrade', null)->all(); // Not upgraded...
        $bf_upgrade = collect($query)->where('dc_upgrade', '!=', null)->where('dc_network', $dc)->all(); // Before upgrade...
        $af_upgrade = collect($query)->where('dc_upgrade', '!=', null)->where('dc_upgrade', $dc)->all(); // After upgrade...

        // Millenium sponsoring...
        $data['mill'] = collect($query)->where('is_direct', '>=', 1)->where('type', '<=', 0)->values()->all(); // Millenium 2K direct sponsors...
        $data['millLower'] = collect($query)->where('is_lower', '>=', 1)->where('type', '<=', 0)->values()->all(); // Millenium 2K lower sponsors...

        // Binary Pairing and Sponsoring...
        foreach (['lft','rgt','direct'] as $c) {
            $cond = 'is_'.$c;
            
            // 3K Binary
            $type1 = [];
            $type1[] = collect($no_upgrade)->where($cond, '!=', 0)->where('type', 1)->all();
            $type1[] = collect($bf_upgrade)->where($cond, '!=', 0)->where('type', 2)->all();
            $data[$c.'1'] = collect($type1)->collapse()->all();

            // 7K Binary
            $type2 = [];
            $type2[] = collect($no_upgrade)->where($cond, '!=', 0)->where('type', 2)->all();
            $type2[] = collect($af_upgrade)->where($cond, '!=', 0)->where('type', 2)->all();
            $data[$c.'2'] = collect($type2)->collapse()->all();
        }

        return $data;
    }
}