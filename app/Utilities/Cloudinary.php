<?php

namespace App\Utilities;

use Cloudinary\Api\Upload\UploadApi;
use Cloudinary\Configuration\Configuration;
use Cloudinary\Api\Admin\AdminApi;

class Cloudinary
{
    public function __construct()
    {
        Configuration::instance([
            'cloud' => [
              'cloud_name' => env('CLOUDINARY_CLOUD_NAME'), 
              'api_key' => env('CLOUDINARY_API_KEY'), 
              'api_secret' => env('CLOUDINARY_API_SECRET')
            ],
            'url' => [
              'secure' => true
            ]
        ]);
    }

    public function uploadImage($image, $folder)
    {
        $publicId = time();
        $base64 = 'data:image/'.$image->getClientOriginalExtension().';base64,'.base64_encode(file_get_contents($image));

        try {
            (new UploadApi())->upload($base64, [
                'resource_type' => 'image',
                'folder' => $folder,
                'public_id' => $publicId
            ]);

        } catch (\Exception $e) {
            return null;
        }
        
        return (new AdminApi())->asset($folder.$publicId);
    }

    public function images($prefix)
    {
        return (new AdminApi())->assets([
            'type' => 'upload',
            'prefix' => $prefix,
            'max_results' => 50
        ]);
    }
}