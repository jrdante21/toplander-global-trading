<?php

namespace App\Utilities;
use App\Models\MembersPurchases;
use App\Models\MembersNetwork;
use App\Models\MembersNetworkUnilevel;

class ProfitUnilevel
{
    private $date = '';
    private $wave = 3;
    private $minimum = 2800;
    private $maximum = 3600;

    public function updateUplines($ids, $date)
    {
        $this->date = date('Y-m-d', strtotime($date));
        $downlines = MembersNetwork::whereIn('id', $ids)->get();

        $uplines = [];
        foreach ($downlines as $q) {
            $uplines[] = explode('.', $q->b_nodes);
            $uplines[] = explode('.', $q->s_nodes);
        }
        $uplines = collect($uplines)->collapse()->filter()->unique()->values();
        return $this->__updateMembers($ids);
    }

    private function __updateMembers($ids)
    {
        $dateID = date('ym', strtotime($this->date));
        $query = MembersPurchases::sumPurchases()
        ->groupBy('members_network_id')
        ->havingRaw('amount_discounted >= '.$this->minimum)
        ->whereIn('members_network_id', $ids)
        ->whereYearMonth($this->date)
        ->get();

        $data = [];
        foreach ($query as $q) {
            // Match to upadate or insert
            $rowMatch['members_network_id'] = $q->members_network_id;
            $rowMatch['date_id'] = $dateID;
            $rowMatch['wave'] = $this->wave;

            // Rows to be updated or inserted
            $toUpdate = $this->__downlinesPurchases($q->members_network_id); // Level 2-10
            $toUpdate['lvl1'] = $this->__compute(1, $q->amount_discounted); // Level 1
            $toUpdate['total_amount'] = $q->amount_discounted;
            $data[] = $toUpdate;

            // Update
            MembersNetworkUnilevel::updateOrInsert($rowMatch, $toUpdate);
        }

        return $data;
    }

    private function __downlinesPurchases($id)
    {
        $query = MembersNetwork::collectUnilevel($id, $this->date)->havingRaw("amount >= ".$this->minimum)->get();

        $data = collect($query)->groupBy('count_nodes')->all();
        $computed = [];
        foreach ($data as $dk => $items) {
            $level = $dk + 2;
            $total = 0;
            foreach ($items as $i) { 
                $total += $this->__compute($level, $i['amount']); 
            }
            $computed['lvl'.$level] = $total;
        }

        return $computed;
    }

    private function __compute($level, $amount)
    {
        if ($amount < $this->minimum) return 0;

        $total = 0;
        switch ($level) {
            case 1:
                $total += ($amount >= $this->maximum) ? 500 : 100;
                break;

            case 2:
                $total += ($amount >= $this->maximum) ? 100 : 50;
                break;

            default:
                $total += 50;
                break;
        }
        
        return $total;
    }
}
