<?php

namespace App\Utilities;

use Hashids\Hashids;
class Hashid
{
    public function __construct()
    {
        $this->hashids = new Hashids(env('HASHIDS_SALT'), 20, 'QWERTYUIOPASDFGHJKLZXCVBNMabcdefghijklmnopqrstuvwxyz1234567890');
    }

    public function encode($str='')
    {
        if (!is_numeric($str)) return null;
        return $this->hashids->encode($str);
    }

    public function decode($str='')
    {
        $hash = $this->hashids->decode($str);
        return (count($hash) >= 1) ? $hash[0] : null; 
    }
}