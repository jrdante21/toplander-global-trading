<?php

namespace App\Utilities;
use Illuminate\Support\Str;
use App\Utilities\Hashid;
use App\Models\MembersNetwork;

class Genealogy
{
    private $binaryData = [];
    private $unilevelData = [];
    private $cssStyles = [
        'container' => "inline-block shadow-md rounded p-2 leading-none",
        'border' => "border-gray-600 border-solid",
    ];

    public function binaryGenealogy($id)
    {
        $query = MembersNetwork::upline()->maintenance()->latestPairings()
        ->withFullname()
        ->whereRaw("b_nodes LIKE CONCAT('%.',?,'.%') OR members_network.id = ?", [$id, $id])
        ->where('type', '>=', 1)
        ->orderByBinaryNodes()
        ->limit(50)
        ->get();

        $this->binaryData = $query;
        return $this->__recursiveBinaryTree($id);
    }

    public function unilevelGenealogy($id)
    {
        $main = MembersNetwork::withFullname()->countDirects()->find($id);

        $dlsSubQuery = MembersNetwork::selectRaw("
            id as sub_id, username, halfname, sponsored_by, dls_count, created_at, type
        ")
        ->withFullname()
        ->countDirects();

        $dlsQuery = MembersNetwork::selectRaw("
            sub.username, sub.halfname, sub.sponsored_by, sub.dls_count, sub.created_at, sub.type
        ")
        ->collectUnilevel($id)
        ->leftJoinSub($dlsSubQuery, 'sub', function($join){
            $join->on('members_network.id', '=', 'sub.sub_id');
        })
        ->orderByDesc('dls_count')
        ->get();

        return collect($dlsQuery)->prepend($main)->all();
    }

    private function __recursiveBinaryTree($id)
    {
        $data = collect($this->binaryData)->firstWhere('id', $id);
        $noneDiv = "<div class='{$this->cssStyles['container']} mx-2 bg-gray-300'>None</div>";

        if (empty($data) && empty($id)) return $noneDiv;

        $hashid = (new Hashid())->encode($id);
        if (empty($data)) return "
            <button value='{$hashid}' class='button mx-2'>View</button>
        ";

        $bgColors = [
            1 => "bg-blue-500 text-white",
            2 => "bg-yellow-500 text-white",
            3 => "bg-red-500 text-white",
        ];

        $details = '';
        foreach ([
                ['title' => 'Date', 'value'=> $data->created_at],
                ['title' => 'Package', 'value'=> $data->type_str],
                ['title' => 'Sponsor', 'value'=> "{$data->upline_sponsore_username} <br> <small>{$data->upline_sponsore_name}</small>"],
                ['title' => '3K Pair', 'value'=> (!empty($data->latest_versus_1)) ? $data->latest_versus_1 : '0 VS 0'],
                ['title' => '7K Pair', 'value'=> (!empty($data->latest_versus_2)) ? $data->latest_versus_2 : '0 VS 0'],
            ] as $d) {
            $details .= "
                <tr>
                    <td valign='top' class='pr-3'>{$d['title']}</td>
                    <td valign='top' class='uppercase font-medium collapse'>{$d['value']}</td>
                </tr>
            ";
        }

        $mainDiv = "
            <tr>
                <td colspan='2' align='center' valign='top' style='line-height: 0px;'>
                    <div class='{$this->cssStyles['container']} {$bgColors[$data->status]} mx-5 dropdown'>
                        <b class='pb-4 cursor-pointer' data-copytext='{$data->username}'>{$data->username}</b>
                        <br>
                        <small>{$data->halfname}</small>
                        <div class='content p-2 z-50'>
                            <table class='text-black border-separate'>{$details}</table>
                        </div>
                    </div>
                    <div><button value='{$data->hashid}' class='button'>&#8659;</button></div>
                    <div><div class='inline-block h-3 border {$this->cssStyles['border']}'></div></div>
                <td>
            </tr>
            <tr>
                <td align='right' class='w-1/2' style='line-height: 0px;'><div class='w-1/2 inline-block h-30px border-t-2 border-l-2 {$this->cssStyles['border']}'></div></td>
                <td align='left' class='w-1/2' style='line-height: 0px;'><div class='w-1/2 inline-block h-30px border-t-2 border-r-2 {$this->cssStyles['border']}'></div></td>
            </tr>
        ";

        $lftDiv = (!empty($data->lft_leg)) ? $this->__recursiveBinaryTree($data->lft_leg) : $noneDiv;
        $rgtDiv = (!empty($data->rgt_leg)) ? $this->__recursiveBinaryTree($data->rgt_leg) : $noneDiv;
        $mainDiv .= "
            <tr>
                <td valign='top' align='center' style='line-height: 0px;'>{$lftDiv}</td>
                <td valign='top' align='center' style='line-height: 0px;'>{$rgtDiv}</td>
            <tr>
        ";

        return "<table class='w-full border-collapse' cellpadding='0' cellspacing='0'>$mainDiv</table>";
    }
}