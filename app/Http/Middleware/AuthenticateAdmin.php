<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $guard = auth()->guard('admin');
        if ($guard->check() && !empty($guard->user()->is_active)) {
            return $next($request);
        }

        return redirect()->route('admin.logout');
    }
}
