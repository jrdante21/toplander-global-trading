<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthenticateAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role)
    {
        $member = auth()->guard('member');
        $admin = auth()->guard('admin');
        if (!$member->check() && !$admin->check()) return response('Log in session timed out.', 401);

        $isValid = false;
        if ($role == 'member') {
            $isValid = true;
            if ($member->check()) $request->merge(['id' => $member->user()->hashid]);

        } else {
            $admin = $admin->user();
            $request->merge(['admin_id' => $admin->id]);

            if ($admin->is_super >= 1) {
                $isValid = true;

            } else {
                $level = [
                    'admin1' => 0,
                    'admin2' => 1,
                    'admin3' => 2,
                ];
                $isValid = (isset($level[$role]) && $admin->level >= $level[$role]) ? true : false;
            }
        }

        if ($isValid) {
            return $next($request);
        } else {
            return response('Authorization not allowed.', 403);
        }
        
    }
}
