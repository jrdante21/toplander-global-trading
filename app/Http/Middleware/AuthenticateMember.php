<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthenticateMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $guard = auth()->guard('member');
        if ($guard->check() && count($guard->user()->networks) >= 1) {
            return $next($request);
        }

        return redirect()->route('member.logout');
    }
}
