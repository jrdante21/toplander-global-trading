<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

class AdminController extends Controller
{
    public function loginPage()
    {
        if (auth()->guard('admin')->check()) return redirect()->route('admin.panel');
        return view('pages.admin.login');
    }

    public function loginForm(LoginRequest $request)
    {
        $req = $request->validated();

        if (auth()->guard('admin')->attempt(['username' => $req['username'], 'password' => $req['password'], 'is_active' => 1])) {
            auth()->guard('member')->logout();
            return redirect()->route('admin.panel');

        } else {
            return back()
                ->withInput($request->only('username'))
                ->with('errors', ["Username or Password is incorrect."]);
        }
    }

    public function panel()
    {
        return view('pages.admin.panel');
    }

    public function forms()
    {
        return view('pages.admin.forms');
    }

    public function genealogy($id)
    {
        return view('pages.member.genealogy', ['id' => $id]);
    }

    public function logout()
    {
        auth()->guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
