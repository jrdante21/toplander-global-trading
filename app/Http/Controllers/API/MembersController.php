<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use App\Utilities\Hashid;

use App\Models\Members;
use App\Models\MembersPayouts;
use App\Models\MembersPurchases;

use Illuminate\Http\Request;
use \App\Http\Requests\ProfileMemberRequest;
use \App\Http\Requests\UsernameRequest;
use \App\Http\Requests\PasswordChangeRequest;

class MembersController extends Controller
{
    public function member(Request $request)
    {
        $id = (new Hashid())->decode($request->id); // Members id
        if (empty($id)) return response('ID not found.', 404);

        $query = Members::with('networks')
        ->benefits('birthday')
        ->benefits('hospital')
        ->totalProfit()
        ->find($id);
        $query->fees = Members::currentProfitFees($id);
        $query->requests = MembersPayouts::where('members_id', $id)->pendingRequests()->get();

        return $query;
    }

    public function memberBenefits(Request $request)
    {
        $id = (new Hashid())->decode($request->id); // Members id
        if (empty($id)) return response('ID not found.', 404);

        $year = $request->input('year', date('Y'));
        $query = Members::benefits('birthday', $year)->benefits('hospital', $year)->findOrFail($id);
        return $query;
    }

    public function search(Request $request)
    {   
        $data = $request->all();
        $query = Members::limit(20);
        if (!empty($data['search'])) {
            $s = '%'.$data['search'].'%';
            $query->whereRaw("(
                CONCAT(fname, ' ', mname, ' ', lname) LIKE ? OR
                CONCAT(fname, ' ', lname) LIKE ? OR
                CONCAT(lname, ' ', fname) LIKE ? OR
                username LIKE ? OR
                account_id LIKE ?
            )", [$s, $s, $s, $s, $s]);
        }

        $query = $query->get();
        return $query;
    }

    public function list(Request $request)
    {
        switch ($request->sort_by) {
            case 'binary_profit':
                $query = Members::totalProfitOfType('binary', $request->profit_year)->orderByDesc('total_profit_binary');
                break;

            case 'unilevel_profit':
                $query = Members::totalProfitOfType('unilevel', $request->profit_year)->orderByDesc('total_profit_unilevel');
                break;

            case 'purchases':
                $query = Members::totalPurchases($request->profit_year)->orderByDesc('total_purchases_count');
                break;
            
            default:
                $query = Members::orderByDesc('created_at');
                if (!empty($request->dates)) {
                    $dates = json_decode($request->dates, true);
                    if (!empty($dates['start']) && !empty($dates['end'])) $query->rangeByDate($dates['start'], $dates['end']);
                }
                break;
        }

        if (!empty($request->address)) $query->whereAddress(json_decode($request->address, true));
        $query = $query->paginate(15);
        return $query;
    }

    public function changePassword(PasswordChangeRequest $request)
    {
        $req = $request->validated();

        $member = auth()->guard('member')->user();
        if (!Hash::check($req['password_old'], $member->password)) return response([
            'message' => "Incorrect Old Password",
            'errors' => [
                'password_old' => ['Old password is incorrect']
            ]
        ], 422);

        $q = Members::find($member->id);
        $q->password = Hash::make($req['password']);
        $q->save();
        return $q;
    }
    
    public function edit(Request $request, $type)
    {
        $id = $request->id;
        if (empty($id)) return response('ID not found.', 404);

        $q = Members::find($id); // Get member
        if (empty($q)) return response('Member not found.', 404);

        switch ($type) {
            case 'password':
                $q->password = Hash::make($request->password);
                break;

            case 'username':
                $request = app()->make(UsernameRequest::class);
                $req = $request->validated();

                $uname = Str::of($req['username'])->trim();
                $users = Members::where('username', $uname)->where('id', '!=', $id)->count();
                if ($users >= 1) return response([
                    'message' => "Invalid Username",
                    'errors' => [
                        'username' => ['Username already taken']
                    ]
                ], 422);
        
                $q->username = $uname;
                break;

            default:
            case 'profile':
                $request = app()->make(ProfileMemberRequest::class);
                $req = $request->validated();

                $q->fname = Str::of($req['fname'])->title()->trim();
                $q->mname = Str::of($req['mname'])->title()->trim();
                $q->lname = Str::of($req['lname'])->title()->trim();
                $q->suffix = $req['suffix'];
                $q->gender = $req['gender'];
                $q->bday = date('Y-m-d', strtotime($req['bday']));
                $q->contact_num = $req['contact_num'];
                $q->province = $req['province'];
                $q->city = $req['city'];
                $q->barangay = $req['barangay'];
                break;
        }

        $q->save();
        return $q;
    }
}
