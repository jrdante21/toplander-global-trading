<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Admins;
use \App\Http\Requests\UsernameRequest;
use \App\Http\Requests\PasswordChangeRequest;
use \App\Http\Requests\ProfileAdminRequest;

class AdminController extends Controller
{
    public function all()
    {
        $query = Admins::where('is_super', 0)->get();
        return $query;
    }

    public function createOrUpdate(ProfileAdminRequest $request)
    {
        $req = $request->validated();
        $q = (!empty($request->id)) ? Admins::findOrFail($request->id) : new Admins;

        $count = Admins::where('username', $req['username']);
        if (!empty($q->id)) $count->where('id', '!=', $q->id);
        $count = $count->count();
        if ($count >= 1) return response("Username already taken.", 422);

        $q->username = $req['username'];
        $q->fname = $req['fname'];
        $q->mname = $req['mname'];
        $q->lname = $req['lname'];
        $q->level = $req['level'];
        if (empty($q->id)) {
            $q->password = Hash::make('12345');
            $q->is_active = 1;
        }
        $q->save();
    }

    public function resetPassword(Request $request)
    {
        $q = Admins::findOrFail($request->id);
        $q->password = Hash::make('12345');
        $q->save();
    }

    public function deactivateOrActivate(Request $request)
    {
        $q = Admins::findOrFail($request->id);
        $act = ($q->is_active >= 1) ? 0: 1;
        if ($act <= 0 && $q->is_super >= 1) return response("Super admin cannot be deactivated", 422);

        $q->is_active = $act;
        $q->save();
    }

    public function editUsername(UsernameRequest $request)
    {
        $req = $request->validated();
        $q = Admins::findOrFail($request->admin_id);
        
        $count = Admins::where('username', $req['username'])->where('id', '!=', $q->id)->count();
        if ($count >= 1) return response("Username already taken.", 422);

        $q->username = $req['username'];
        $q->save();
        return $q;
    }

    public function changePassword(PasswordChangeRequest $request)
    {
        $req = $request->validated();
        $q = Admins::findOrFail($request->admin_id);

        if (!Hash::check($req['password_old'], $q->password)) return response([
            'message' => "Incorrect Old Password",
            'errors' => [
                'password_old' => ['Old password is incorrect']
            ]
        ], 422);

        $q->password = Hash::make($req['password']);
        $q->save();
        return $q;
    }
}
