<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use \App\Http\Requests\UsernameRequest;

use App\Models\Members;
use App\Models\MembersNetwork;
use App\Utilities\Hashid;
use App\Utilities\Genealogy;
use App\Utilities\ProfitBinary;

class MembersNetworkController extends Controller
{
    public function genealogy(Request $request)
    {
        $netID = (new Hashid())->decode($request->networking_id);
        if (empty($netID)) return response('ID not found', 404);

        $genealogy = new Genealogy();
        return ($request->type == 'unilevel') ? $genealogy->unilevelGenealogy($netID) : $genealogy->binaryGenealogy($netID);
    }

    public function downlinesPairing(Request $request)
    {
        $id = $request->networking_id;
        if (empty($id)) return response('ID not found.', 404);

        $query = MembersNetwork::upline()->maintenance()->latestPairings()
        ->whereRaw("b_nodes LIKE CONCAT('%.',?,'.%') OR members_network.id = ?", [$id, $id])
        ->where('type', '>=', 1)
        ->limit(50)
        ->orderByBinaryNodes()
        ->get();

        return $query;
    }

    public function editUsername(UsernameRequest $request)
    {
        $q = MembersNetwork::findOrFail($request->networking_id);

        $req = $request->validated();
        $uname = Str::of($req['username'])->trim();
        $users = MembersNetwork::where('username', $uname)->where('id', '!=', $request->networking_id)->count();
        if ($users >= 1) return response([
            'message' => "Invalid Username",
            'errors' => [
                'username' => ['Username already taken']
            ]
        ], 422);

        $q->username = $uname;
        $q->save();
        return $q;
    }

    public function modify(Request $request, $type)
    {
        $q = MembersNetwork::findOrFail($request->networking_id);
        switch ($type) {
            case 'sponsor':
                $sponsor = MembersNetwork::where('username', $request->sponsor)->where('sponsored_by', '!=', $q->sponsored_by)->first();
                if (empty($sponsor)) return response([
                    'message' => "Invalid sponsor",
                    'errors' => [
                        'sponsor' => ['Sponsor not found.']
                    ]
                ], 422);

                $result = $this::updateSponsor($q, $sponsor);
                if (!$result) return response("This account's sponsor can't be change.", 422);
                return $result;
                break;

            case 'transfer':
                $account = Members::where('username', $request->account)->where('id', '!=', $q->members_id)->first();
                if (empty($account)) return response([
                    'message' => "Invalid Account",
                    'errors' => [
                        'account' => ['Main account not found.']
                    ]
                ], 422);

                $q->members_id = $account->id;
                $q->save();
                return $q;
                break;
            
            default:
                return response('Request type unknown', 404);
                break;
        }
    }

    public function refreshPairings(Request $request)
    {
        $ids = $request->ids;
        if (empty($ids) && !is_array($ids)) return response("No ids found.", 422);

        $datetime = (empty($request->datetime)) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($request->datetime));
        $type = $request->input('type', 1);
        return (new ProfitBinary())->refreshMembers($ids, $datetime, $type);
    }

    private static function updateSponsor($network, $newSponsor)
    {
        $downlines = MembersNetwork::where('sponsored_by', $network->id)->count();
        if ($downlines >= 1) return false;

        $prevSponsors = $network->s_nodes;
        $network->sponsored_by = $newSponsor->id;
        $network->s_nodes = '.'.$newSponsor->id.$newSponsor->s_nodes;

        $nodes = [explode('.', $prevSponsors), explode('.', $network->s_nodes)];
        $nodes = collect($nodes)->collapse()->filter()->unique()->values();
        DB::beginTransaction();
        try {
            $network->save();
            $result = (new ProfitBinary())->refreshMembers($nodes, $network->created_at, $network->type);
            
            DB::commit();
            return $result;

        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }
}
