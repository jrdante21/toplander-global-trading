<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Utilities\Hashid;

use App\Http\Requests\PurchaseRequest;
use App\Models\MembersPurchases;
use App\Utilities\ProfitUnilevel;

class PurchasesController extends Controller
{
    public function member(Request $request)
    {
        $id = (new Hashid())->decode($request->id); // Members id
        if (empty($id)) return response('ID not found.', 404);

        // Queries
        $total = MembersPurchases::sumPurchases()->joinMembersNetwork()->where('members_id', $id);
        $query = MembersPurchases::selectRaw("
            DATE(members_purchases.confirmed_at) as date,
            GROUP_CONCAT(members_purchases.id) as ids,
            members_purchases.confirmed_at
        ")
        ->sumPurchases()
        ->joinMembersNetwork()
        ->groupBy('date')
        ->orderByDesc('date')
        ->where('members_id', $id);

        // Date Range
        if (!empty($request->dates)) {
            $dates = json_decode($request->dates, true);
            if (!empty($dates['start']) && !empty($dates['end'])) {
                $total->rangeByDate($dates['start'], $dates['end'], 'confirmed_at');
                $query->rangeByDate($dates['start'], $dates['end'], 'confirmed_at');
            }
        }

        // Execute
        $total = $total->first();
        $query = $query->paginate(10);

        // Items
        $ids = [];
        foreach ($query as $q) { $ids[] = explode(',', $q->ids); }
        $ids = collect($ids)->collapse()->all();
        $query2 = MembersPurchases::selectRaw("
            members_purchases.id,
            members_network.username,
            DATE(members_purchases.confirmed_at) as date,
            members_purchases.quantity,
            members_purchases.price_discounted,
            members_purchases.products_id,
            members_purchases.created_at,
            members_purchases.ar_num,
            members_purchases.admins_id
        ")
        ->with(['product','admin'])
        ->joinMembersNetwork()
        ->whereIn('members_purchases.id', $ids)
        ->get();
        foreach ($query as $q) { $q->items = collect($query2)->where('date', $q->date)->values()->all(); }

        $query = collect($query)->put('total_sum', $total->amount_discounted)->all();
        return $query;
    }

    public function list(Request $request)
    {
        $total = MembersPurchases::selectRaw("SUM(quantity * price_discounted) as total_sum")->address();
        $query = MembersPurchases::selectRaw("
            members_network.members_id,
            CONCAT(members.fname, ' ', members.lname) as name,
            CONCAT(members.barangay, ', ', members.city, ', ', members.province) as address,
            GROUP_CONCAT(members_purchases.id) as purchases_ids,
            SUM(quantity * price_discounted) as total_amount,
            DATE(members_purchases.created_at) as purchase_date
        ")
        ->joinMembersNetwork()
        ->join('members', 'members_network.members_id', '=', 'members.id')
        ->groupByRaw('members_network.members_id, purchase_date')
        ->orderByDesc('members_purchases.created_at');

        // Address
        if (!empty($request->address)) {
            $where = [];
            $addr = json_decode($request->address, true);
            if (!empty($addr['province'])) $where[] = ['province', '=', $addr['province']];
            if (!empty($addr['city'])) $where[] = ['city', '=', $addr['city']];
            if (!empty($addr['barangay'])) $where[] = ['barangay', '=', $addr['barangay']];

            if (!empty($where)) {
                $total->where($where);
                $query->where($where);
            }
        }

        // Date Range
        if (!empty($request->dates)) {
            $dates = json_decode($request->dates, true);
            if (!empty($dates['start']) && !empty($dates['end'])) {
                $total->rangeByDate($dates['start'], $dates['end']);
                $query->rangeByDate($dates['start'], $dates['end']);
            }
        }

        $hashid = new Hashid();
        $query = $query->paginate(15);
        foreach ($query as $q) {
            $q->name = Str::of($q->name)->title();
            $q->address = Str::of($q->address)->title();
            $q->hashid = $hashid->encode($q->members_id);

            $ids = explode(',', $q->purchases_ids);
            $q->items = MembersPurchases::selectRaw("
                members_purchases.id,
                members_network.username,
                members_purchases.confirmed_at as date,
                members_purchases.created_at as date_added,
                members_purchases.quantity,
                members_purchases.price_discounted,
                members_purchases.products_id,
                members_purchases.created_at,
                members_purchases.ar_num,
                members_purchases.admins_id
            ")
            ->with(['product','admin'])
            ->joinMembersNetwork()
            ->whereIn('members_purchases.id', $ids)
            ->get();
        }
        
        $total = $total->first();
        $query = collect($query)->put('total_sum', $total->total_sum)->all();
        return $query;
    }

    public function add(PurchaseRequest $request)
    {
        DB::beginTransaction();
        try {
            $req = $request->validated();
    
            $items = [];
            foreach ($req['items'] as $i) {
                $i['requested_at'] = $req['date'];
                $i['confirmed_at'] = $req['date'];
                $i['created_at'] = date('Y-m-d H:i:s');
                $i['ar_num'] = $req['ar_num'];
                $i['status'] = 1;
                $i['admins_id'] = $request->admin_id;
                $items[] = $i;
            }

            // Insert Purchases
            MembersPurchases::insert($items);

            // Update Unilevel
            $ids = collect($req['items'])->pluck('members_network_id');
            $data = (new ProfitUnilevel())->updateUplines($ids, $req['date']);

            DB::commit();
            return $data;

        } catch (\Exception $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }

    public function delete(Request $request)
    {
        if (empty($request->id)) return response('ID not found.', 404);
        $q = MembersPurchases::find($request->id);
        if (empty($q)) return response('Purchase not found.', 404);

        DB::beginTransaction();
        try {
            $date = $q->confirmed_at;
            $id = $q->members_network_id;

            $q->delete();
            (new ProfitUnilevel())->updateUplines([$id], $date);
            DB::commit();
            return response("Purchases deleted successfully", 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }
}
