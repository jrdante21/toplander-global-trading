<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Codes;
use App\Models\CodesRemarks;

class CodesController extends Controller
{
    public function all(Request $request)
    {
        $query = Codes::with(['remarks','admin'])
        ->member()
        ->orderByRaw('IF(updated_at > created_at, updated_at, created_at) DESC');

        // Search
        if (!empty($request->search)) $query->where('code', 'like', '%'.$request->search.'%');

        // Date Range
        if (!empty($request->dates)) {
            $dates = json_decode($request->dates, true);
            if (!empty($dates['start']) && !empty($dates['end'])) $query->whereRaw("
                DATE(created_at) >= ? AND
                DATE(created_at) <= ?
            ", [
                date('Y-m-d', strtotime($dates['start'])),
                date('Y-m-d', strtotime($dates['end'])),
            ]);
        }

        // Type
        if (!empty($request->type)) {
            switch ($request->type) {
                case '2k': $type = 0; break;
                case '3k': $type = 1; break;
                default: $type = 2; break;
            }
            $query->where('type', $type);
        }

        // Is used
        if (!empty($request->used)) {
            if ($request->used <= 1) {
                $query->whereNotNull('members_network_id');
            } else {
                $query->whereNull('members_network_id');
            }
        }

        $query = $query->paginate(15);
        return $query;
    }

    public function generate(Request $request)
    {
        if (empty($request->packages)) return response("No packages to generate", 404);

        $data = [];

        // Generate codes
        do {
            $codes = [];
            $codesData = [];
            foreach ($request->packages as $pk => $p) {
                for ($i=1; $i <= $p; $i++) { 
                    $code = static::generate_str($pk);
                    $codes[] = $code;
                    $codesData[] = [
                        'admins_id' => $request->admin_id,
                        'code' => $code,
                        'type' => $pk,
                    ];
                }
            }
            $duplicates = count(collect($codes)->duplicates()->all());
            $duplicates += Codes::whereIn('code', $codes)->count();
            if ($duplicates <= 0) $data = $codesData;
        } while (count($data) <= 0);

        // Insert codes remarks
        $remarks = new CodesRemarks;
        $remarks->remarks = $request->remarks;
        $remarks->save();

        // Insert many codes
        $remarks->codes()->createMany($data);

        // Return codes
        $data = collect($data)->mapToGroups(function ($item) {
            return [$item['type'] => $item['code']];
        })->all();
        return $data;
    }

    private static function generate_str($type=1)
    {
        $str = str_split('qwertyuiopasdfghjklzxcvbnm1234567890');
        $str = collect($str)->shuffle()->all();
        $str = implode('', $str);
        $str = str_split($str, 5);
        $str = collect($str)->take(3)->all();

        switch ($type) {
            case 0: $str[0] = 'unlvl'; break;
            case 2: $str[0] = 'tgt7k'; break;
        }

        $str = strtoupper(implode('-', $str));
        return $str;
    }
}
