<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utilities\Hashid;
use App\Models\Members;
use App\Models\MembersPayouts;
use App\Models\MembersNetwork;
use App\Models\MembersNetworkPairings;
use App\Models\MembersNetworkUnilevel;

class SummaryController extends Controller
{
    public function adminDashboard()
    {
        $binaryProfit = MembersNetworkPairings::totalForMembers()->first();
        $unilvlProfit = MembersNetworkUnilevel::totalForMembers()->first();
        $totalPayouts = MembersPayouts::totalForMembers()->first();
        $data['total_profits'] = [
            'binary' => $binaryProfit->total_profit_binary,
            'fifthpair' => $binaryProfit->total_profit_fifthpair,
            'unilevel' => $unilvlProfit->total_profit_unilevel
        ];
        $data['total_payouts'] = [
            'binary' => $totalPayouts->total_payout_binary,
            'fifthpair' => $totalPayouts->total_payout_fifthpair,
            'unilevel' => $totalPayouts->total_payout_unilevel
        ];
        $data['count_members'] = Members::count();
        $data['count_networking'] = MembersNetwork::count();
        $data['count_request_payouts'] = MembersPayouts::where('status', 0)->count();
        $data['admin_data'] = auth()->guard('admin')->user();

        return $data;
    }

    public function pairingsSponsorings(Request $request)
    {
        $id = $request->networking_id;
        if (empty($id)) return response('ID not found.', 404);

        $query = MembersNetworkPairings::selectRaw("members_network_id, lft_leg, rgt_leg")
        ->countPairingSponsoring($id)
        ->joinMembersNetwork()
        ->paginate(10);
        
        return response()->json($query);
    }

    public function profits(Request $request)
    {
        $id = (new Hashid())->decode($request->id); // Members id
        if (empty($id)) return response('ID not found.', 404);

        // Profits...
        $type = $request->input('profit_type', 1);
        switch ($type) {
            default: // Binary...
                $profit = MembersNetworkPairings::selectRaw("
                    members_id,
                    members_network_pairings.date as p_date,
                    members_network_pairings.cycle as p_cycle,
                    SUM(b_pts + s_pts + u_pts) as p_amount,
                    GROUP_CONCAT(DISTINCT members_network_pairings.id SEPARATOR '.') as p_ids
                ")
                ->joinMembersNetwork()
                ->whereRaw("(b_pts + s_pts + u_pts) >= 1");

                $stmt_profit = 'binary';
                break;
            
            case 2: // Fifth Pair...
                $profit = MembersNetworkPairings::selectRaw("
                    members_id,
                    members_network_pairings.date as p_date,
                    members_network_pairings.cycle as p_cycle,
                    SUM(fifth * 30) as p_amount,
                    GROUP_CONCAT(DISTINCT members_network_pairings.id SEPARATOR '.') as p_ids
                ")
                ->joinMembersNetwork()
                ->whereRaw("fifth >= 1");

                $stmt_profit = 'fifthpair';
                break;

            case 3: // Unilevel...
                $levels = implode(' + ', MembersNetworkUnilevel::UNILEVELS);
                $profit = MembersNetworkUnilevel::selectRaw("
                    members_id,
                    DATE_FORMAT(STR_TO_DATE(CONCAT(date_id, '01'), '%y%m%d'), '%Y-%m-%d') as p_date,
                    1 as p_cycle,
                    SUM(".$levels.") as p_amount,
                    GROUP_CONCAT(DISTINCT members_network_unilevel.id SEPARATOR '.') as p_ids
                ")
                ->joinMembersNetwork()
                ->whereRaw("(".$levels.") >= 1");

                $stmt_profit = 'unilevel';
                break;
        }
        $profit = $profit->selectRaw("
            'profit' as p_type
        ")
        ->groupByRaw("p_date, p_cycle")
        ->where('members_id', $id);

        // Payouts...
        $payout = MembersPayouts::selectRaw("
            members_id,
            DATE(confirmed_at) as p_date,
            3 as p_cycle, 
            points as p_amount,
            id as p_ids,
            'payout' as p_type
        ")
        ->groupByRaw("p_date, p_cycle")
        ->where('profit_type', $type)
        ->where('members_id', $id);
        
        // Summary...
        $query = $payout->union($profit)->orderByRaw("CONCAT(p_date, '-0', p_cycle) DESC")->paginate(10);
        foreach ($query as $q) {
            // Profits...
            $total_profit_stmt = 'total_profit_'.$stmt_profit;
            $total_profit = ($type <= 2) ? 
                MembersNetworkPairings::totalForMembers()->groupBy('members_id')->whereRaw("CONCAT(date, '-0', cycle) <= ?", [$q->p_date.'-0'.$q->p_cycle]):  
                MembersNetworkUnilevel::totalForMembers()->groupBy('members_id')->where('date_id', '<=', date('ym', strtotime($q->p_date)));
            $total_profit->where('members_id', '=', $q->members_id);
            $total_profit = $total_profit->first();
            $total_profit = (!empty($total_profit)) ? $total_profit->$total_profit_stmt : 0;

            // Payouts...
            $total_payout_stmt = 'total_payout_'.$stmt_profit;
            $total_payout = MembersPayouts::totalForMembers()->groupBy('members_id')->whereRaw("DATE(confirmed_at) <= ? AND members_id = ?", [$q->p_date, $q->members_id]);
            $total_payout = $total_payout->first();
            $total_payout = (!empty($total_payout)) ? $total_payout->$total_payout_stmt : 0;

            // Add to data...
            $q->total_profit = $total_profit;
            $q->total_payout = $total_payout;
            $q->total_balance = $total_profit - $total_payout;
            if ($q->p_type == 'payout') {
                $q->p_data = MembersPayouts::find($q->p_ids);

            } else {
                $ids = explode('.', $q->p_ids);
                $q->p_data = static::profitsInfo($ids, $type);
            }

            // Date...
            $q->p_date = date('M d, Y', strtotime($q->p_date));
        }

        return response()->json($query);
    }

    private static function profitsInfo($ids, $type=1)
    {
        $data = [];

        if ($type <= 2) {
            $query = MembersNetworkPairings::selectRaw("
                username, b_pts, s_pts, u_pts, (fifth * 30) as fifth,
                members_network_pairings.type,
                (CASE members_network_pairings.type WHEN 1 THEN '3K' ELSE '7K' END) as type_str
            ")
            ->joinMembersNetwork()
            ->whereIn('members_network_pairings.id', $ids)
            ->get();

            $types = ($type <= 1) ? [
                // Binary
                ['name'=>'Pairing', 'value'=>'b_pts'],
                ['name'=>'Sponsoring', 'value'=>'s_pts'],
                ['name'=>'Millennium Sponsoring', 'value'=>'u_pts'],
            ]:[
                // Fifth Pair
                ['name'=>'Fifth Pair', 'value'=>'fifth'],
            ];

            foreach ($query as $q) {
                foreach ($types as $t) {
                    $type = $t['value'];
                    if (!empty(ceil($q->$type))) {
                        $data[] = [
                            'amount' => $q->$type,
                            'username' => $q->username,
                            'package' => ($t['value'] != 'u_pts') ? $q->type_str.' '.$t['name'] : $t['name'],
                        ];
                    }
                }
            }

        } 
        else {
            $data = MembersNetworkUnilevel::joinMembersNetwork()->whereIn('members_network_unilevel.id', $ids)->get();
        }

        return $data;
    }
}
