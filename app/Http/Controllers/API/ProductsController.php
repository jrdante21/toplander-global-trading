<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\ProductsDescriptions;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductDescriptionRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\ImageURLRequest;
use App\Utilities\Cloudinary;

class ProductsController extends Controller
{

    public function all()
    {
        $data['products'] = Products::with('products_descriptions')->orderByRaw('IFNULL(updated_at, published) DESC')->get();
        $data['descriptions'] = ProductsDescriptions::get();
        return $data;
    }

    public function list()
    {
        return Products::get();
    }

    public function descriptions()
    {
        return ProductsDescriptions::get();
    }

    public function createUpdateProduct(ProductRequest $request)
    {
        $req = $request->validated();
        $q = (!empty($request->id)) ? Products::findOrFail($request->id) : new Products;
        $q->name = $req['name'];
        $q->products_descriptions_id = (empty($req['products_descriptions_id'])) ? null : $req['products_descriptions_id'];
        $q->acronym = $req['acronym'];
        $q->category = $req['category'];
        $q->price = $req['price'];
        $q->price_discounted = $req['price_discounted'];
        if (empty($request->id)) $q->published = 0;
        $q->save();
    }

    public function publishOrUnpublish(Request $request)
    {
        $q = Products::findOrFail($request->id);

        $publish = ($q->published >= 1) ? 0 : 1;
        if (!empty($publish)) {
            if (empty($q->image) || empty($q->products_descriptions_id)) return response("Product information is incomplete. It cannot be published.", 422);
        }

        $q->published = $publish;
        $q->save();
    }

    public function createUpdateDescription(ProductDescriptionRequest $request)
    {
        $req = $request->validated();
        $q = (!empty($request->id)) ? ProductsDescriptions::findOrFail($request->id) : new ProductsDescriptions;
        $q->name = $req['name'];
        $q->description = $req['description'];
        $q->save();
    }

    public function uploadImage(ImageUploadRequest $request)
    {
        $query = Products::findOrFail($request->id);
        $req = $request->validated();

        $result = (new Cloudinary())->uploadImage($request->image, 'products/');
        if (is_null($result)) response("Something went wrong", 500);

        $query->image = $result['secure_url'];
        $query->save();
    }

    public function changeImageUrl(ImageURLRequest $request)
    {
        $query = Products::findOrFail($request->id);
        $req = $request->validated();
        $query->image = $req['image'];
        $query->save();
    }

    public function images()
    {
        $images = (new Cloudinary())->images("products");
        return $images['resources'];
    }
}
