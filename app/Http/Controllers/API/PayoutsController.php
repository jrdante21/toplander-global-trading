<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Utilities\Hashid;
use App\Models\Members;
use App\Models\MembersPayouts;

class PayoutsController extends Controller
{
    public function member(Request $request)
    {
        $id = (new Hashid())->decode($request->id); // Members id
        if (empty($id)) return response('ID not found.', 404);

        $type = $request->input('type');
        $date = $request->input('date');
        $date = date('Y-m-d', strtotime($date));

        $query = MembersPayouts::with('admin')->where('members_id', $id)->where('status', 1)->orderByDesc('confirmed_at');
        if (!empty($type)) $query->where('profit_type', $type);
        if (!empty($date)) $query->whereRaw('DATE(confirmed_at) <= ?', [$date]);

        $query = $query->paginate(10);
        return $query;
    }

    public function requested(Request $request)
    {
        $query = MembersPayouts::with('members')->address()->where('status', 0)->orderByDesc('requested_at')->get();

        $total = collect($query)->reduce(function ($carry, $item) {
            return $carry + ($item->points - $item->fee);
        });
        $query = collect($query)->groupBy('province')->all();
        return [
            'data' => $query,
            'total' => $total
        ];
    }

    public function confirmed(Request $request)
    {
        $query = MembersPayouts::with(['admin', 'members'])->orderByDesc('confirmed_at');
        $total = MembersPayouts::selectRaw("SUM(points) as total_sum");

        $where = [['status', '=', 1]];
        if (!empty($request->profit_type)) $where[] = ['profit_type', '=', $request->profit_type];
        if (!empty($request->received_via)) $where[] = ['received_via', '=', $request->received_via];

        // Date Range
        if (!empty($request->dates)) {
            $dates = json_decode($request->dates, true);
            if (!empty($dates['start']) && !empty($dates['end'])) {
                $query->rangeByDate($dates['start'], $dates['end']);
                $total->rangeByDate($dates['start'], $dates['end']);
            }
        }

        // Address
        $address = (!empty($request->address)) ? json_decode($request->address, true) : [];
        $query->address($address)->where($where);
        $total->address($address)->where($where);

        $total = $total->first();
        $query = $query->paginate(15);
        $query = collect($query)->put('total_sum', $total->total_sum)->all();
        return $query;
    }

    public function unpaids(Request $request)
    {
        switch ($request->profit_type) {
            case 'unilevel':
                $profitType = 'total_available_unilevel';
                break;

            case 'fifthpair':
                $profitType = 'total_available_fifthpair';
                break;
            
            case 'binary':
            default:
                $profitType = 'total_available_binary';
                break;
        }

        $query = Members::totalProfit()
        ->orderByDesc($profitType)
        ->where($profitType, '>=', 1);

        if (!empty($request->address)) $query->whereAddress(json_decode($request->address, true));

        $query = $query->paginate(15);
        // $total = Members::selectRaw("SUM($profitType) as total_sum")->totalProfit()->first();
        // $query = collect($query)->put('total_sum', $total->total_sum)->all();
        return $query;
    }

    public function request(Request $request)
    {
        $data = $request->all();
        $id = (new Hashid())->decode($request->id); // Members id
        if (empty($id)) return response('ID not found.', 404);

        $data['members_id'] = $id;

        switch ($data['profit_type']) {
            case 1:
                $sTime = date('ymd1200', strtotime('sunday this week'));
                $eTime = date('ymd1700', strtotime('monday this week'));
                $cTime = date('ymdHi');

                if ($cTime < $sTime && $cTime > $eTime) return response("Payout schedule timed out. Payout schedule every Sunday 12pm to Monday 5pm.", 422);
                break;
            
            default:
                $eTime = date('ym07');
                $cTime = date('ymd');

                if ($cTime > $eTime) return response("Payout schedule timed out. Payout schedule every 1st week of the month.", 422);
                break;
        }

        if (!empty($data['bankname'])) $data['remarks'] = $data['bankname'].' | '.$data['banknumber'].' | '.$data['remarks'];
        $data['admin'] = null;
        $data['status'] = 0;
        return static::save($data);
    }

    public function confirm(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) return response('ID not found.', 404);

        $q = MembersPayouts::find($id);
        if (empty($q)) return response('Data not found.', 404);
        if ($q->status >= 1) return response('Payout already confirmed.', 422);

        $q->confirmed_at = date('Y-m-d H:i:s');
        $q->status = 1;
        $q->confirmed_by = $request->admin_id;
        $q->save();
        return $q;
    }

    public function add(Request $request)
    {
        $data = $request->all();
        $id = $request->input('members_id'); // Members ID...
        if (empty($id)) return response('ID not found.', 404);

        $data['admin'] = $request->admin_id;
        return static::save($data);
    }

    public function edit(Request $request)
    {
        $data = $request->input('data');
        $id = $request->input('id');
        if (empty($id)) return response('ID not found.', 404);

        $q1 = MembersPayouts::find($id);
        if (empty($q1)) return response('Data not found.', 404);

        $points = ($data['points'] - $q1->points);
        if (!empty($points)) {
            if (static::hasPreviousPayouts($id, $q1->profit_type, $q1->members_id)) return response("Points can't be changed.", 422);

            $balance = static::getAvailableBalance($q1->members_id, $q1->profit_type);
            if (!empty($q1->status)) $balance += $q1->points;
            if ($data['points'] > $balance) return response("Insufficient available balance.", 422);
        }

        foreach ($data as $dk => $d) {
            $q1->$dk = $d;
        }
        $q1->confirmed_by = $request->admin_id;
        $q1->save();

        return $q1;
    }

    public function delete(Request $request)
    {
        $id = $request->input('id');
        if (empty($id)) return response('ID not found.', 404);

        $q1 = MembersPayouts::find($id);
        if (empty($q1)) return response('Data not found.', 404);
        if ($q1->status >= 1 && (date('Ym') - date('Ym', strtotime($q1->confirmed_at))) > 1) return response("Payout is above 1 month from now, it can't be deleted.", 422);
        if (static::hasPreviousPayouts($id, $q1->profit_type, $q1->members_id)) return response("This can't be deleted.", 422);

        $q1->delete();
        return $q1;
    }

    private static function save($data)
    {
        $pendings = MembersPayouts::where('members_id', $data['members_id'])->pendingRequests($data['profit_type'])->count();
        if ($pendings >= 1) return response('Pending requests confirmation needed.', 422);

        $available = static::getAvailableBalance($data['members_id'], $data['profit_type']);
        if (empty($available) || $data['points'] > $available) return response("Insufficient available balance.", 422);

        $fees = Members::currentProfitFees($data['members_id']);
        switch ($data['profit_type']) {
            case 2: $fees = $fees['fifthpair']; break;
            case 3: $fees = $fees['unilevel']; break;
            default: $fees = $fees['binary']; break;
        }
        $fees = (!empty($fees)) ? $fees['ids'] : '';

        $q = new MembersPayouts;
        $q->members_id = $data['members_id'];
        $q->points = $data['points'];
        $q->profit_type = $data['profit_type'];
        $q->accounts = $fees;
        $q->remarks = $data['remarks'];
        $q->status = $data['status'];
        $q->received_via = $data['received_via'];

        $date = date('Y-m-d H:i:s');
        $q->requested_at = $date;
        $q->confirmed_at = (empty($data['status'])) ? null : $date;
        $q->confirmed_by = $data['admin'];
        $q->save();

        return $q;
    }

    private static function getAvailableBalance($id, $profit_type=1)
    {
        $query = Members::totalProfit()->find($id);
        switch ($profit_type) {
            case 2: $profit_type = 'fifthpair'; break;
            case 3: $profit_type = 'unilevel'; break;
            default: $profit_type = 'binary';  break;
        }
        $available = 'total_available_'.$profit_type;
        return $query->$available;
    }

    private static function hasPreviousPayouts($id, $type, $members_id)
    {
        $count = MembersPayouts::where([
            ['id', '>', $id],
            ['profit_type', '=', $type],
            ['members_id', '=', $members_id]
        ])->count();

        return ($count >= 1) ? true : false;
    }
}
