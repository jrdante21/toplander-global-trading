<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use \App\Http\Requests\ProfileMemberRequest;
use \App\Http\Requests\UsernameRequest;
use \App\Http\Requests\MemberNetworkRequest;
use \App\Http\Requests\MemberNetworkToplessRequest;

use App\Models\Codes;
use App\Models\Members;
use App\Models\MembersNetwork;
use App\Models\MembersNetworkUpgrades;
use App\Utilities\ProfitBinary;

class RegistrationController extends Controller
{
    public function levelUp(Request $request)
    {
        $username = MembersNetwork::where('username', $request->username)->where('type', 1)->first();
        $code = Codes::notUsed()->where('code', $request->code)->where('type', 2)->first();

        $errors = [];
        if (empty($username)) $errors['username'] = ['Username not found'];
        if (empty($code)) $errors['code'] = ['Code is invalid'];
        if (!empty($errors)) {
            return response([
                'message' => 'Invalid data given',
                'errors'=> $errors
            ], 422);
        }

        DB::beginTransaction();
        try {
            $username->type = 2;
            $username->save();

            $code->members_network_id = $username->id;
            $code->save();

            $upgrade = new MembersNetworkUpgrades;
            $upgrade->members_network_id = $username->id;
            $upgrade->type = 2;
            $upgrade->save();

            (new ProfitBinary())->updateUplines($username->id, $upgrade->created_at);

            DB::commit();
            return $username;

        } catch (\Exception $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }

    public function searchAccount(Request $request)
    {
        $search = $request->search;
        $query = Members::where('username', $search)->orWhere('account_id', $search)->first();
        if (empty($query)) {
            return response([
                'message' => 'No account found',
                'errors'=> ['No account found']
            ], 404);
        }

        return $query;
    }

    public function checkProfile(ProfileMemberRequest $request)
    {
        $request = $request->validated();
        return Members::whereProfile($request)->first();
    }

    public function checkLogin(UsernameRequest $request)
    {
        $request = $request->validated();
        if (Members::where('username', $request['username'])->count() >= 1) {
            return response([
                'message' => 'Invalid data given',
                'errors'=> ['username' => ['Username has already been taken']]
            ], 422);
        }
        return $request;
    }

    public function checkNetwork(MemberNetworkRequest $request)
    {
        $request = $request->validated();
        $request = MembersNetwork::validate($request);
        $errors = MembersNetwork::$validationErrors;
        if (!empty($errors)) {
            return response([
                'message' => 'Invalid data given',
                'errors'=> $errors
            ], 422);
        }
        return $request;
    }

    public function register(Request $request)
    {
        $isTopless = (empty($request->is_topless)) ? false : true;

        DB::beginTransaction();

        try {
            // Networking Account & Code Validation
            $networkForm = ($isTopless) ? app()->make(MemberNetworkToplessRequest::class)->validated() : app()->make(MemberNetworkRequest::class)->validated();

            $networkValidated = MembersNetwork::validate($networkForm, $isTopless);
            $networkErrors = MembersNetwork::$validationErrors;
            if (!empty($networkErrors)) return response([
                'message' => 'Invalid data given',
                'errors'=> $networkErrors
            ], 422);

            // New Account
            if (empty($request->id)) {
                // Profile
                $profileForm =  app()->make(ProfileMemberRequest::class);
                $profile = $profileForm->validated();
                if (!empty(Members::whereProfile($profile)->first())) return response("Profile already exist", 422);
    
                // Username
                $usernameForm = app()->make(UsernameRequest::class);
                $username = $usernameForm->validated();
                if (Members::where('username', $username['username'])->count() >= 1) return response([
                    'message' => 'Invalid data given',
                    'errors'=> [
                        'username' => ["Username already taken"]
                    ]
                ], 422);
    
                // Save Profile
                $memberProfile = new Members;
                $memberProfile->account_id = Members::generateAccountID();
                $memberProfile->username = $username['username'];
                $memberProfile->password = Hash::make($request->password);
                $memberProfile->fname = $profile['fname'];
                $memberProfile->mname = $profile['mname'];
                $memberProfile->lname = $profile['lname'];
                $memberProfile->suffix = $request->suffix;
                $memberProfile->gender = $profile['gender'];
                $memberProfile->contact_num = $profile['contact_num'];
                $memberProfile->bday = date('Y-m-d', strtotime($profile['bday']));
                $memberProfile->barangay = $profile['barangay'];
                $memberProfile->city = $profile['city'];
                $memberProfile->province = $profile['province'];
                $memberProfile->country = 'PH';
                $memberProfile->save();
    
                // Set ID
                $memberProfileID = $memberProfile->id;
    
            // Old Account
            } else {
                $memberProfile = Members::findOrFail($request->id);
                $memberProfileID = $memberProfile->id;
            }
            
            // Save networking information
            $networkID = static::saveMembersNetworking($networkValidated, $memberProfileID, $isTopless);
            
            // Update Binary
            if (!$isTopless) (new ProfitBinary())->updateUplines($networkID);

            DB::commit();
            return $memberProfile;

        } catch (\Exception $e) {
            DB::rollback();
            return response("Something went wrong ".$e, 500);
        }
    }

    private static function saveMembersNetworking($data, $memberID, $isTopless=false)
    {
        // Network
        $network = new MembersNetwork;
        $network->members_id = $memberID;
        $network->username = $data['network_username'];
        $network->type = $data['package'];

        if (!$isTopless) {
            if ($data['package'] >= 1) {
                $network->placed_in = $data['placement'];
                $network->b_nodes = $data['b_nodes'];
            } else {
                $network->b_nodes = '.';
            }

            $network->sponsored_by = $data['sponsor'];
            $network->s_nodes = $data['s_nodes'];

        } else {
            $network->b_nodes = '.';
            $network->s_nodes = '.';
        }

        $network->save();

        // Placement
        if (!$isTopless) {
            if ($data['package'] >= 1) {
                $position = $data['position'];
                $placement = MembersNetwork::find($data['placement']);
                $placement->$position = $network->id;
                $placement->save();
            }
        }

        // Code
        $code = Codes::find($data['code']);
        $code->members_network_id = $network->id;
        $code->save();

        return $network->id;
    }
}
