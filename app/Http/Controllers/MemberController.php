<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;

class MemberController extends Controller
{
    public function loginPage()
    {
        if (auth()->guard('member')->check()) return redirect()->route('member.dashboard');
        return view('pages.member.login');
    }

    public function loginForm(LoginRequest $request)
    {
        $req = $request->validated();

        if (auth()->guard('member')->attempt(['username' => $req['username'], 'password' => $req['password']])) {
            auth()->guard('admin')->logout();
            return redirect()->route('member.dashboard');

        } else {
            return back()
                ->withInput($request->only('username'))
                ->with('errors', ["Username or Password is incorrect."]);
        }
    }

    public function dashboard()
    {
        $data['member'] = auth()->guard('member')->user();
        return view('pages.member.dashboard', $data);
    }

    public function genealogy()
    {
        $data['id'] = auth()->guard('member')->user()->hashid;
        return view('pages.member.genealogy', $data);
    }

    public function forms()
    {
        $data['member'] = auth()->guard('member')->user();
        return view('pages.member.forms', $data);
    }

    public function logout()
    {
        auth()->guard('member')->logout();
        return redirect()->route('member.login');
    }
}
