<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Products;
use App\Utilities\Hashid;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;

class SiteController extends Controller
{   
    protected $details = [];

    public function __construct()
    {
        $this->details = app_details();
    }

    public function index()
    {
        $this->setSEO(
            $this->details['title'].' - '.$this->details['introduction']['main']['header'],
            implode(' ', $this->details['introduction']['main']['description'])
        );
        $data['products'] = Products::with('products_descriptions')->whereIn('id', [2, 5, 10])->get();
        return view('pages.site.home', $data);
    }

    public function aboutUs()
    {
        $this->setSEO('About Our Company', implode(' ', $this->details['about_us']));
        return view('pages.site.about-us');
    }

    public function products($id='')
    {
        $products = Products::with('products_descriptions')->where('published', 1)->get();
        $data['products'] = collect($products)->groupBy('category')->all();

        $id = (new Hashid())->decode($id);
        if (!empty($id)) {
            $product = collect($products)->firstWhere('id', $id);
            $this->setSEO(
                $product->name,
                $product->products_descriptions->description,
                $product->image,
                explode(' ', $product->name)
            );
            $data['product'] = $product;
            return view('pages.site.product', $data);

        } else {
            $this->setSEO(
                $this->details['introduction']['products']['header'],
                implode(' ', $this->details['introduction']['products']['description'])
            );
            return view('pages.site.products', $data);
        }
    }

    public function marketing()
    {
        return view('pages.site.marketing');
    }

    public function credential($cred='')
    {
        if (!view()->exists('pages.site.credentials.'.$cred)) return redirect()->route('site.home');

        $data['slug'] = $cred;
        $data['name'] = Str::headline($cred);
        return view('pages.site.credential', $data);
    }

    public function register()
    {
        return view('pages.site.register');
    }

    private function setSEO($title, $description, $image='', $addKeywords=[])
    {
        SEOTools::setTitle($title);
        SEOTools::setDescription($description);

        $keywords = explode(' ', $this->details['title']);
        $keywords[] = $title;
        $keywords[] = $this->details['title'];
        $keywords = collect($keywords)->merge($addKeywords)->all();
        SEOMeta::addKeyword($keywords);

        if (empty($image)) $image = url('images/logo.png');
        OpenGraph::addImage($image);
        TwitterCard::setImage($image);
    }
}
