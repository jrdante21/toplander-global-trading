<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberNetworkToplessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'network_username' => 'required|min:5|alpha_num',
            'package' => 'required',
            'code' => 'required|min:15'
        ];
    }

    public function attributes()
    {
        return [
            'network_username' => 'username'
        ];
    }
}
