<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'date' => date('Y-m-d H:i:s', strtotime($this->date))
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'items' => 'required|array',
            'date' => 'required|date|before_or_equal:'.now()->format('M d Y'),
            'ar_num' => 'required|alpha_num'
        ];
    }

    public function attributes()
    {
        return [
            'ar_num' => 'AR number'
        ];
    }
}
