<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = "regex:/^[A-Za-z- ]+$/i";
        return [
            'fname' => 'required|min:2|'.$regex,
            'mname' => 'required|min:2|'.$regex,
            'lname' => 'required|min:2|'.$regex,
            'suffix' => 'sometimes',
            'gender' => 'required|numeric',
            'bday' => 'required|date',
            'contact_num' => 'required|min:10|numeric',
            'province' => 'required',
            'city' => 'required',
            'barangay' => 'required',
            'password' => 'sometimes',
        ];
    }

    public function attributes()
    {
        return [
            'fname' => 'first name',
            'mname' => 'middle name',
            'lname' => 'last name',
            'contact_num' => 'contact number',
            'bday' => 'birthdate',
            'city' => 'municipality'
        ];
    }
}
