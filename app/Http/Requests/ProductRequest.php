<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'acronym' => 'required',
            'category' => 'required',
            'products_descriptions_id' => 'sometimes|required|numeric',
            'price' => 'required|numeric',
            'price_discounted' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'price' => 'SRP',
            'price_discounted' => 'DP',
        ];
    }
}
