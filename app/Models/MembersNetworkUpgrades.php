<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembersNetworkUpgrades extends Model
{
    use HasFactory;
    public $table = 'members_network_upgrades';
}
