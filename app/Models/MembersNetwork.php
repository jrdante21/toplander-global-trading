<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Utilities\Hashid;

class MembersNetwork extends Model
{
    use HasFactory;
    public $table = 'members_network';
    protected $appends = [
        'hashid',
        'cycle',
        'type_str',
        'position', // Include local scope upline...
        'is7th', 'status', // Include local scope maintenance...
    ];
    
    public function getHashidAttribute()
    {
        return (new Hashid())->encode($this->id);
    }

    public function getCreatedAtAttribute($value)
    {
        return date('M d, Y h:i A', strtotime($value));
    }

    public function getCycleAttribute()
    {
        $time = date('Hi', strtotime($this->created_at));
        return ($time <= 1200) ? 1 : 2;
    }

    public function getTypeStrAttribute()
    {
        switch ($this->type) {
            default: return 'Millennium 2K'; break;
            case 1: return 'Binary 3K'; break;
            case 2: return 'Binary 7K'; break;
        }
    }

    public function getPositionAttribute()
    {
        if (!is_null($this->upline_lft) || !is_null($this->upline_rgt)) {
            return ($this->upline_lft == $this->id) ? 'left' : 'right';
        
        } else {
            return null;
        }
    }

    public function getIs7thAttribute()
    {
        if (empty($this->maintenance_main_nodes)) return false;

        $nodes_exp = explode(',', $this->maintenance_main_nodes);
        $nodes_data = [];
        foreach ($nodes_exp as $n) {
            $n = explode(':', $n);
            $nodes_data[] = [
                'node_id' => intval((!empty($n[0])) ? $n[0] : 0),
                'length' => intval((!empty($n[1])) ? $n[1] : 0),
            ];
        }

        $nodes_data = collect($nodes_data)->sortBy('length')->values()->take(7)->all();
        $is7th = collect($nodes_data)->firstWhere('node_id', $this->id);
        return (!is_null($is7th)) ? true : false;
    }

    public function getStatusAttribute()
    {
        if ($this->type <= 0) return 1;

        $add_30_days = date('Y-m-t', strtotime('+30 days', strtotime($this->created_at)));
        $current_date = date('Y-m-d');
        $min = $this->maintenance_minimum;

        if ($add_30_days >= $current_date) {
            if ($current_date >= date('Y-m-15', strtotime($add_30_days))) {
                if ($this->is7th) {
                    if ($this->maintenance_self_curr < $min && $this->maintenance_main_curr < 2750) return 2;
                } else {
                    if ($this->maintenance_self_curr < $min) return 2;
                }

                return 1;
            } else {
                return 1;
            }

        } else {

            if ($this->is7th) {
                if ($this->maintenance_self_last < $min && $this->maintenance_main_last < 2750) return 3;
            } else {
                if ($this->maintenance_self_last < $min) return 3;
            }
            
            if (date('d') >= 15) {
                if ($this->is7th) {
                    if ($this->maintenance_self_curr < $min && $this->maintenance_main_curr < 2750) return 2;
                } else {
                    if ($this->maintenance_self_curr < $min) return 2;
                }

                return 1;
            } else {
                return 1;
            }
        }
    }

    public function members()
    {
        return $this->belongsTo(Members::class);
    }

    public function purchases()
    {
        return $this->hasMany(MembersPurchases::class);
    }

    public function scopeOrderByBinaryNodes($query)
    {
        return $query->orderByRaw("(LENGTH(b_nodes) - LENGTH(REPLACE(b_nodes, '.', '')))");
    }

    public function scopeUpline($query)
    {
        $place = $this::selectRaw("
            members_network.id as placed_in,
            members_network.username as upline_placement_username,
            CONCAT(m.fname, ' ', m.mname, ' ', m.lname, ' ', IFNULL(m.suffix, '')) as upline_placement_name,
            members_network.lft_leg as upline_lft,
            members_network.rgt_leg as upline_rgt
        ")
        ->join('members as m', 'members_network.members_id', '=', 'm.id');

        $spon = $this::selectRaw("
            members_network.id as sponsored_by,
            members_network.username as upline_sponsore_username,
            CONCAT(m.fname, ' ', m.mname, ' ', m.lname, ' ', IFNULL(m.suffix, '')) as upline_sponsore_name
        ")
        ->join('members as m', 'members_network.members_id', '=', 'm.id');

        return $query
        ->leftJoinSub($place, 'upline_placement', function($join){
            $join->on('members_network.placed_in', '=', 'upline_placement.placed_in');
        })
        ->leftJoinSub($spon, 'upline_sponsore', function($join){
            $join->on('members_network.sponsored_by', '=', 'upline_sponsore.sponsored_by');
        });
    }

    public function scopeMaintenance($query, $date='')
    {
        $date = (!empty($date)) ? strtotime($date) : strtotime(date('Y-m-d'));
        $year = date('Y', $date);
        $curr_month = date('Y-m', $date);
        $last_month = date('Y-m', strtotime('-1 month', $date));

        // Current and Last Month purchases...
        $purch = [];
        foreach (['self', 'main'] as $p) {
            $purch[$p] = MembersPurchases::selectRaw("
                members_network_id as maintenance_".$p."_id,
                SUM(CASE WHEN (DATE_FORMAT(confirmed_at, '%Y-%m') = ?) THEN quantity * price_discounted ELSE 0 END) as maintenance_".$p."_curr,
                SUM(CASE WHEN (DATE_FORMAT(confirmed_at, '%Y-%m') = ?) THEN quantity * price_discounted ELSE 0 END) as maintenance_".$p."_last
            ", [$curr_month, $last_month])
            ->groupBy('maintenance_'.$p.'_id')
            ->rangeByMonth($last_month, $curr_month);
        }

        // Maint networking account...
        $main = $this::selectRaw("
            members_id as maintenance_main_id, 
            MIN(id) as maintenance_main_network_id,
            GROUP_CONCAT(
                CONCAT(id, ':',
                    (LENGTH(b_nodes) - LENGTH(REPLACE(b_nodes, '.', '')))
                )
            ) as maintenance_main_nodes
        ")
        ->where('type', '>=', 1)
        ->groupBy('maintenance_main_id');

        // Minimum maintenance...
        $min1 = ($year >= 2021) ? 275 : 250;
        $min2 = ($year >= 2021) ? 550 : 500;
        $minimum = $this::selectRaw("
            members_network.id as minimum_id,
            (CASE
                (CASE
                    WHEN (members_network_upgrades.id IS NOT NULL) THEN
                        (CASE WHEN (? >= DATE_FORMAT(members_network_upgrades.created_at, '%Y-%m')) THEN 2 ELSE 1 END)
                ELSE members_network.type END)
                WHEN 1 THEN ?
                WHEN 2 THEN ?
            ELSE 0 END) as maintenance_minimum
        ", [$curr_month, $min1, $min2])
        ->leftJoin('members_network_upgrades', function($join){
            $join->on('members_network.id', '=', 'members_network_upgrades.members_network_id');
        });

        return $query
        ->leftJoinSub($main, 'main', function($join){
            $join->on('members_network.members_id', '=', 'main.maintenance_main_id');
        })
        ->leftJoinSub($minimum, 'minimum', function($join){
            $join->on('members_network.id', '=', 'minimum.minimum_id');
        })
        ->leftJoinSub($purch['main'], 'maintenance_main', function($join){
            $join->on('main.maintenance_main_network_id', '=', 'maintenance_main.maintenance_main_id');
        })
        ->leftJoinSub($purch['self'], 'maintenance_self', function($join){
            $join->on('members_network.id', '=', 'maintenance_self.maintenance_self_id');
        });
    }

    public function scopeLatestPairings($query, $date='', $cycle=2)
    {
        $pair1 = MembersNetworkPairings::latestPairings(1, $date, $cycle);
        $pair2 = MembersNetworkPairings::latestPairings(2, $date, $cycle);
        return $query
        ->leftJoinSub($pair1, 'latest_pairings1', function($join){
            $join->on('members_network.id', '=', 'latest_pairings1.members_network_id');
        })
        ->leftJoinSub($pair2, 'latest_pairings2', function($join){
            $join->on('members_network.id', '=', 'latest_pairings2.members_network_id');
        });
    }

    public function scopeCountDirects($query)
    {
        $downlines_count = $this::selectRaw("sponsored_by as dls_sID, COUNT(*) as dls_count")
        ->groupBy('sponsored_by');

        return $query
        ->leftJoinSub($downlines_count, 'dls_count', function($join){
            $join->on('members_network.id', '=', 'dls_count.dls_sID');
        });
    }

    public function scopeWithFullname($query)
    {
        $fullname = Members::selectRaw("
            id as fullname_id,
            CONCAT(fname, ' ', mname, ' ', lname, ' ', IFNULL(suffix, '')) as fullname,
            CONCAT(fname, ' ', lname, ' ', IFNULL(suffix, '')) as halfname
        ");

        return $query
        ->leftJoinSub($fullname, 'fullname', function($join){
            $join->on('members_network.members_id', '=', 'fullname.fullname_id');
        });
    }

    public function scopeCollectUnilevel($query, $id, $date='')
    {
        $id = '.'.$id.'.';
        $trim_nodes = "SUBSTRING_INDEX(s_nodes, '$id', 1)";

        $purchase_this_month = MembersPurchases::sumPurchases()
        ->groupBy('members_network_id')
        ->whereYearMonth($date);

        return $query->selectRaw("
            id,
            $trim_nodes as nodes,
            (LENGTH($trim_nodes) - LENGTH(REPLACE($trim_nodes, '.', ''))) as count_nodes,
            IFNULL(purchased.amount_discounted, 0) as amount
        ")
        ->orderBy('count_nodes')
        ->havingRaw('count_nodes <= 8')
        ->leftJoinSub($purchase_this_month, 'purchased', function($join){
            $join->on('members_network.id', '=', 'purchased.members_network_id');
        })
        ->whereRaw("s_nodes LIKE CONCAT('%', ?, '%')", [$id]);
    }

/*
|--------------------------------------------------------------------------
| Static Functions
|--------------------------------------------------------------------------
*/
    public static $validationErrors = [];
    public static function validate($data, $isTopless=false)
    {
        // Username
        if (static::where('username', $data['network_username'])->count() >= 1) {
            static::$validationErrors['network_username'] = ["Username already taken."];
            return false;
        }

        // Code
        $code = Codes::notUsed()->where('type', $data['package'])->where('code', $data['code'])->first();
        if (empty($code)) {
            static::$validationErrors['code'] = ["Code is invalid."];
            return false;
        }
        $data['code'] = $code->id;

        if ($isTopless) return $data;

        // Sponsor
        $sponsor = static::where('username', $data['sponsor'])->first();
        if (empty($sponsor)) {
            static::$validationErrors['sponsor'] = ["Sponsor not found."];
            return false;
        }
        $data['sponsor'] = $sponsor->id;
        $data['s_nodes'] = '.'.$sponsor->id.$sponsor->s_nodes;

        // Package
        if ($data['package'] >= 1) {
            // Sponsor 2K millennium not allowed
            if ($sponsor->type <= 0) static::$validationErrors['sponsor'] = ["Sponsor is not allowed to this package."];

            // Placement
            $placement = static::where('username', $data['placement'])->first();
            if (empty($placement)) {
                static::$validationErrors['placement'] = ["Placement not found."];
                return false;
            }
            $data['placement'] = $placement->id;
            $data['b_nodes'] = $placement->b_nodes.$placement->id.'.';

            // Position
            $position = ($data['position'] <= 1) ? 'lft_leg' : 'rgt_leg';
            if (!empty($placement->$position)) static::$validationErrors['position'] = ["Position already occupied."];
            $data['position'] = $position;

            // Sponsor aligned to placement
            if ($placement->id != $sponsor->id && !Str::contains($placement->b_nodes, '.'.$sponsor->id.'.')) {
                static::$validationErrors['placement'] = ["Placement is not allowed."];
            }
        }

        return $data;
    }
}
