<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsDescriptions extends Model
{
    use HasFactory;
    public $table = 'products_descriptions';
    public $timestamps = false;
    public $appends = ['html'];

    public function getHtmlAttribute()
    {
        $descs = preg_split('/\r\n|\r|\n/', $this->description);
        return collect($descs)->reduce(function ($carry, $item) {
            $p = (!empty($item)) ? "<p>{$item}</p>" : "<br>";
            return $carry .= $p;
        });
    }
}
