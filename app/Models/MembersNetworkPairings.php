<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembersNetworkPairings extends Model
{
    use HasFactory;
    public $table = 'members_network_pairings';
    public $timestamps = false;
    protected $appends = ['date_format', 'versus'];

    public function getDateFormatAttribute()
    {
        return date('M d, Y', strtotime($this->date));
    }

    public function getVersusAttribute()
    {
        return "{$this->lft} vs {$this->rgt}";
    }

    public function scopeLatestPairings($query, $type=1, $date='', $cycle=2)
    {   
        $date = (!empty($date)) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        $dc_stmt = "CONCAT(date, '-0', cycle)";

        $type_join = $this::selectRaw("members_network_id, MAX($dc_stmt) as dc")
            ->groupBy('members_network_id')
            ->where('type', $type)
            ->whereRaw("$dc_stmt <= ?", [$date.'-0'.$cycle]);

        return $query->selectRaw("
            members_network_pairings.members_network_id,
            type.dc as latest_date_$type,
            lft as latest_lft_$type,
            rgt as latest_rgt_$type,

            (CASE 
                WHEN (lft > rgt) THEN CONCAT((lft - rgt), ' vs 0')
                WHEN (lft < rgt) THEN CONCAT('0 vs ', (rgt - lft))
            ELSE '0 vs 0' END) as latest_versus_$type,

            paired as latest_paired_$type
        ")
        ->groupBy('members_network_id')
        ->leftJoinSub($type_join, 'type', function($join){
            $join->on('members_network_pairings.members_network_id', '=', 'type.members_network_id');
        })
        ->whereRaw("$dc_stmt = type.dc AND type = ?", [$type]);
    }

    public function scopeJoinMembersNetwork($query)
    {
        return $query->join('members_network', function($join){
            $join->on('members_network_pairings.members_network_id', '=', 'members_network.id');
        });
    }

    public function scopeTotalForMembers($query)
    {
        return $query->selectRaw("
            members_id,
            IFNULL(SUM(b_pts) + SUM(s_pts) + SUM(u_pts), 0) as total_profit_binary,
            IFNULL(SUM(fifth * 30), 0) as total_profit_fifthpair
        ")
        ->join('members_network', function($join){
            $join->on('members_network_pairings.members_network_id', '=', 'members_network.id');
        });
    }

    public function scopeCountPairingSponsoring($query, $id)
    {
        $totals = [];
        foreach (['paired','directs','fifth'] as $t) {
            for ($i=1; $i <= 2; $i++) {
                $stmt = "SELECT SUM($t) FROM members_network_pairings WHERE 
                    members_network_id = '$id' AND 
                    CONCAT(date, '-0', cycle) <= date_cycle AND
                    type = $i
                ";
                $totals[] = "($stmt) as $t$i";
            }
        }
        $totals = implode(',', $totals);

        $versus = [];
        for ($i=0; $i <= 2; $i++) { 
            $versus[] = "
                (CASE WHEN members_network_pairings.type = $i THEN CONCAT(lft, ' vs ', rgt) ELSE
                    (SELECT CONCAT(lft, ' vs ', rgt) FROM members_network_pairings WHERE
                        members_network_id = '$id' AND 
                        CONCAT(date, '-0', cycle) <= date_cycle AND
                        type = $i
                    ORDER BY CONCAT(date, '-0', cycle) DESC LIMIT 1)
                END) as versus$i
            ";
        }
        $versus = implode(',', $versus);

        return $query->selectRaw("
            members_network_pairings.id as pair_id,
            members_network_pairings.type as pair_type,
            members_network_pairings.date,
            members_network_pairings.cycle,
            CONCAT(members_network_pairings.date, '-0', members_network_pairings.cycle) as date_cycle,
            members_network_pairings.paired,
            members_network_pairings.directs,
            members_network_pairings.fifth,
            $totals,
            $versus
        ")
        ->where('members_network_pairings.members_network_id', $id)
        ->orderByDesc('date_cycle');
    }
}
