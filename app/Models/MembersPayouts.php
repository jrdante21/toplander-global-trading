<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MembersPayouts extends Model
{
    use HasFactory, SoftDeletes;
    public $table = 'members_payouts';
    protected $appends = ['fee', 'profit_type_str', 'received_via_str'];

    public function getConfirmedAtAttribute($value)
    {
        return date('M d, Y h:i A', strtotime($value));
    }

    public function getRequestedAtAttribute($value)
    {
        return date('M d, Y h:i A', strtotime($value));
    }

    public function getRemarksAttribute($value)
    {
        return ucfirst($value);
    }

    public function getFeeAttribute()
    {
        $ids = explode('.', $this->accounts);
        return count($ids) * 100;
    }

    public function getProfitTypeStrAttribute()
    {
        switch ($this->profit_type) {
            case 1: return 'Binary'; break;
            case 2: return 'Fifth Pair'; break;
            default: return 'Unilevel'; break;
        }
    }

    public function getReceivedViaStrAttribute()
    {
        switch ($this->received_via) {
            case 1: return 'Check'; break;
            case 2: return 'Purchased'; break;
            case 3: return 'Cash'; break;
            case 4: return 'Bank Transfer'; break;
            case 6: return 'Money Transfer'; break;
            default: return 'Other'; break;
        }
    }

    public function admin()
    {
        return $this->belongsTo(Admins::class, 'confirmed_by');
    }

    public function members()
    {
        return $this->belongsTo(Members::class);
    }

    public function scopeAddress($query, $address=[])
    {
        $addr = Members::selectRaw("
            id as address_id,
            UPPER(province) as province,
            city,
            barangay
        ")
        ->groupBy('id');

        $query->leftJoinSub($addr, 'address', function ($join) {
            $join->on('members_payouts.members_id', '=', 'address.address_id');
        });

        if (!empty($address['province'])) $query->where('province', $address['province']);
        if (!empty($address['city'])) $query->where('city', $address['city']);
        if (!empty($address['barangay'])) $query->where('barangay', $address['barangay']);

        return $query;
    }

    public function scopePendingRequests($query, $profit_type=0)
    {
        if (!empty($profit_type)) $query->where('profit_type', $profit_type);
        return $query->where('status', 0);
    }
    
    public function scopeTotalForMembers($query)
    {
        return $query->selectRaw("
            members_id,
            IFNULL(SUM(CASE WHEN profit_type = 1 THEN points ELSE 0 END), 0) as total_payout_binary,      
            IFNULL(SUM(CASE WHEN profit_type = 2 THEN points ELSE 0 END), 0) as total_payout_fifthpair,      
            IFNULL(SUM(CASE WHEN profit_type = 3 THEN CEIL(points) ELSE 0 END), 0) as total_payout_unilevel      
        ")
        ->where('status', 1);
    }

    public function scopeRangeByDate($query, $start='', $end='')
    {
        if (empty($start) && empty($end)) return $query;
        return $query
        ->whereRaw("DATE(confirmed_at) >= ? && DATE(confirmed_at) <= ?", [
            date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))
        ]);
    }
}
