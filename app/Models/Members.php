<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Utilities\Hashid;

class Members extends Authenticatable
{
    use HasFactory, Notifiable;
    public $table = 'members';
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['created_at_str', 'hashid', 'fullname', 'halfname', 'gender_str', 'address', 'is_password_default'];

    public function getProvinceAttribute($value)
    {
        return trim(strtoupper($value));
    }

    public function getCityAttribute($value)
    {
        return trim(strtoupper($value));
    }

    public function getBarangayAttribute($value)
    {
        return trim(strtoupper($value));
    }

    public function getBdayAttribute($value)
    {
        return date('M d, Y', strtotime($value));
    }

    public function getCreatedAtStrAttribute()
    {
        return date('M d, Y h:i A', strtotime($this->created_at));
    }

    public function getHashidAttribute()
    {
        return (new Hashid())->encode($this->id);
    }

    public function getFullnameAttribute()
    {
        return Str::of("{$this->fname} {$this->mname} {$this->lname} {$this->suffix}")->title();
    }

    public function getHalfnameAttribute()
    {
        return Str::of("{$this->fname} {$this->lname}")->title();
    }

    public function getGenderStrAttribute()
    {
        switch ($this->gender) {
            default: return 'Male'; break;
            case 2: return 'Female'; break;
        }
    }

    public function getAddressAttribute()
    {
        $addr = collect([
            $this->barangay,
            $this->city,
            $this->province
        ])->filter()->all();
        return Str::of(implode(', ', $addr))->title();
    }

    public function getIsPasswordDefaultAttribute()
    {
        return password_verify('12345', $this->password);
    }

    public function networks()
    {
        return $this->hasMany(MembersNetwork::class)->upline()->maintenance();
    }

    public function scopeWhereProfile($query, $data)
    {
        return $query->where([
            ['fname', '=', $data['fname']],
            ['mname', '=', $data['mname']],
            ['lname', '=', $data['lname']],
            ['bday', '=', date('Y-m-d', strtotime($data['bday']))]
        ]);
    }

    public function scopeTotalProfit($query)
    {
        $pairings = MembersNetworkPairings::totalForMembers()->groupBy('members_id'); // Binary and Fifth Pair Profits...
        $unilevel = MembersNetworkUnilevel::totalForMembers()->groupBy('members_id'); // Unilevel Profits...
        $payouts = MembersPayouts::totalForMembers()->groupBy('members_id'); // Payouts for all profits...

        $total = $this::selectRaw("
            members.id as total_profit_id,

            total_profit_binary,
            total_payout_binary,
            (total_profit_binary - IFNULL(total_payout_binary, 0)) as total_available_binary,

            total_profit_fifthpair,
            total_payout_fifthpair,
            (total_profit_fifthpair - IFNULL(total_payout_fifthpair, 0)) as total_available_fifthpair,

            total_profit_unilevel,
            total_payout_unilevel,
            (total_profit_unilevel - IFNULL(total_payout_unilevel, 0)) as total_available_unilevel
        ")
        ->leftJoinSub($pairings, 'pairings', function($join){
            $join->on('members.id', '=', 'pairings.members_id');
        })
        ->leftJoinSub($unilevel, 'unilevel', function($join){
            $join->on('members.id', '=', 'unilevel.members_id');
        })
        ->leftJoinSub($payouts, 'payouts', function($join){
            $join->on('members.id', '=', 'payouts.members_id');
        });

        return $query
        ->leftJoinSub($total, 'total_profit', function($join){
            $join->on('members.id', '=', 'total_profit.total_profit_id');
        });
    }

    public function scopeTotalProfitOfType($query, $type, $year='')
    {
        if ($type == 'binary') {
            $profit = MembersNetworkPairings::totalForMembers();
            if (!empty($year)) $profit->whereYear('date', $year);
        } else {
            $profit = MembersNetworkUnilevel::totalForMembers();
            if (!empty($year)) $profit->whereRaw("
                DATE_FORMAT(STR_TO_DATE(CONCAT(date_id, '01'), '%y%m%d'), '%Y') = ?
            ", [$year]);
        }

        $profit = $profit->groupBy('members_id');
        return $query
        ->leftJoinSub($profit, 'profit', function($join){
            $join->on('members.id', '=', 'profit.members_id');
        });
    }

    public function scopeBenefits($query, $type, $year='')
    {
        if (empty($year)) $year = date('Y');
        $subYear = $year - 1;

        if ($type == 'hospital') {
            $type = 'benefits_hospital';
            $startYear = "DATE_FORMAT(members.created_at, '{$subYear}-%m-%d')";
            $endedYear = "DATE_FORMAT(DATE_SUB(DATE_FORMAT(members.created_at, '{$year}-%m-%d'), INTERVAL 1 DAY), '%Y-%m-%d')";

        } else {
            $type = 'benefits_birthday';
            $startYear = "DATE_FORMAT(members.bday, '{$subYear}-%m-%d')";
            $endedYear = "DATE_FORMAT(DATE_SUB(DATE_FORMAT(members.bday, '{$year}-%m-%d'), INTERVAL 1 DAY), '%Y-%m-%d')";
        }

        // Purchases
        $purchase = MembersNetwork::selectRaw("
            members.id as {$type}_purchases_id,
            SUM(members_purchases.quantity * members_purchases.price_discounted) as {$type}_purchases_count
        ")
        ->groupBy('members.id')
        ->join('members', 'members_network.members_id', '=', 'members.id')
        ->leftJoin('members_purchases', function ($join) use ($startYear, $endedYear) {
            $join->on('members_network.id', '=', 'members_purchases.members_network_id')
            ->whereRaw("
                DATE(members_purchases.confirmed_at) >= {$startYear} AND
                DATE(members_purchases.confirmed_at) <= {$endedYear}
            ");
        });

        // Directs
        $directs = MembersNetwork::selectRaw("
            members.id as {$type}_directs_id,
            COUNT(reward_directs.id) as {$type}_directs_count,
            {$startYear} as {$type}_date_fr,
            {$endedYear} as {$type}_date_to
        ")
        ->groupBy('members.id')
        ->join('members', 'members_network.members_id', '=', 'members.id')
        ->leftJoin('members_network as reward_directs', function ($join) use ($startYear, $endedYear) {
            $join->on('members_network.id', '=', 'reward_directs.sponsored_by')
            ->where('reward_directs.members_id', '!=', 'members_network.members_id')
            ->whereRaw("
                DATE(reward_directs.created_at) >= {$startYear} AND
                DATE(reward_directs.created_at) <= {$endedYear}
            ");
        });

        return $query
        ->leftJoinSub($purchase, $type.'_purchase', function ($join) use ($type) {
            $join->on('members.id', '=', "{$type}_purchase.{$type}_purchases_id");
        })
        ->leftJoinSub($directs, $type.'_directs', function ($join) use ($type) {
            $join->on('members.id', '=', "{$type}_directs.{$type}_directs_id");
        });
    }

    public function scopeTotalPurchases($query, $year='')
    {
        $purchases = MembersPurchases::selectRaw("
            members_network.members_id as total_purchases_id,
            SUM(quantity * price_discounted) as total_purchases_count
        ")
        ->joinMembersNetwork()
        ->groupBy('total_purchases_id');
        if (!empty($year)) $purchases->whereRaw("YEAR(confirmed_at) = ?", [$year]);

        return $query
        ->leftJoinSub($purchases, 'total_purchases', function ($join) {
            $join->on('members.id', '=', 'total_purchases.total_purchases_id');
        });
    }

    public function scopeWhereAddress($query, $address)
    {
        if (!empty($address['province'])) $query->where('province', $address['province']);
        if (!empty($address['city'])) $query->where('city', $address['city']);
        if (!empty($address['barangay'])) $query->where('barangay', $address['barangay']);
        return $query;
    }

    public function scopeRangeByDate($query, $start='', $end='')
    {
        if (empty($start) && empty($end)) return $query;
        return $query
        ->whereRaw("DATE(created_at) >= ? && DATE(created_at) <= ?", [
            date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))
        ]);
    }

/*
|--------------------------------------------------------------------------
| Static Functions
|--------------------------------------------------------------------------
*/

    public static function currentProfitFees($id)
    {
        $dateIfNull = '2018-01-01';
        $dates = MembersPayouts::selectRaw("
            members_id,
            IFNULL(MAX(CASE WHEN profit_type = 1 THEN DATE(confirmed_at) END), ?) as date_binary,
            IFNULL(MAX(CASE WHEN profit_type = 2 THEN DATE(confirmed_at) END), ?) as date_fifthpair,
            IFNULL(MAX(CASE WHEN profit_type = 3 THEN DATE_FORMAT(confirmed_at, '%y%m') END), ?) as date_unilevel
        ", [$dateIfNull, $dateIfNull, '1801'])
        ->groupBy('members_id')
        ->where('members_id', '=', $id)
        ->first();

        $stmt = "
            GROUP_CONCAT(DISTINCT members_network.id SEPARATOR '.') as ids,
            COUNT(DISTINCT(members_network.id)) * 100 as amount
        ";

        $binaryDate = (!empty($dates)) ? $dates->date_binary : $dateIfNull;
        $binary = MembersNetworkPairings::selectRaw($stmt)
        ->groupBy('members_id')
        ->joinMembersNetwork()
        ->whereRaw("(b_pts + s_pts + u_pts) >= 1")
        ->whereRaw("DATE(members_network_pairings.date) >= ?", [$binaryDate])
        ->where('members_id', '=', $id)
        ->first();
        
        $levels = implode(' + ', MembersNetworkUnilevel::UNILEVELS);
        $unilevelDate = (!empty($dates)) ? $dates->date_unilevel : $dateIfNull;
        $unilevel = MembersNetworkUnilevel::selectRaw($stmt)
        ->groupBy('members_id')
        ->joinMembersNetwork()
        ->whereRaw("(".$levels.") >= 1")
        ->whereRaw("members_network_unilevel.date_id >= ?", [$unilevelDate])
        ->where('members_id', '=', $id)
        ->first();

       return [
            'date_starts' => $dates,
            'binary' => $binary,
            'unilevel' => $unilevel,
            'fifthpair' => ['amount'=>0, 'ids'=>'']
       ];
    }

    public static function generateAccountID()
    {
        $last = static::orderByDesc('created_at')->first();
        $id = Str::substr($last->account_id, 2);
        $id = date('y').$id;
        return $id+=1;
    }
}
