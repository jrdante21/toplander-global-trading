<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admins extends Authenticatable
{
    use HasFactory, Notifiable;
    public $table = 'admins';
    protected $hidden = ['password'];
    protected $appends = ['fullname', 'halfname', 'level_str'];

    public function getFullnameAttribute()
    {
        return ucwords("{$this->fname} {$this->mname} {$this->lname}");
    }

    public function getHalfnameAttribute()
    {
        return ucwords("{$this->fname} {$this->lname}");
    }

    public function getLevelStrAttribute()
    {   
        $levels = [
            0 => 'Viewer',
            1 => 'Admin 1',
            2 => 'Admin 2'
        ];

        return (!empty($levels[$this->level])) ? $levels[$this->level] : $levels[0];
    }
}
