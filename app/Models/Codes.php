<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Codes extends Model
{
    use HasFactory;
    public $table = 'codes';
    protected $appends = ['type_str'];
    protected $guarded = [];

    public function getTypeStrAttribute()
    {
        switch ($this->type) {
            default: return 'Millennium 2K'; break;
            case 1: return 'Binary 3K'; break;
            case 2: return 'Binary 7K'; break;
        }
    }

    public function remarks()
    {
        return $this->belongsTo(CodesRemarks::class, 'remarks_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admins::class, 'admins_id');
    }

    public function scopeNotUsed($query)
    {
        return $query->whereNull('members_network_id');
    }

    public function scopeMember($query)
    {
        $member = MembersNetwork::selectRaw("
            members_network.id as mID,
            LOWER(CONCAT(fname, ' ', mname, ' ', lname)) as name,
            members_network.username
        ")
        ->join('members', 'members_network.members_id', '=', 'members.id');

        return $query->leftJoinSub($member, 'm', function($join){
            $join->on('codes.members_network_id', '=', 'm.mID');
        });
    }
}
