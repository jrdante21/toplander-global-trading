<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Utilities\Hashid;

class Products extends Model
{
    use HasFactory;
    public $table = 'products';
    public $appends = ['image_url', 'hashid'];

    public function getImageUrlAttribute()
    {
        $image = $this->image;
        if (empty($image)) return "/images/no-image.jpg";
        return $image;
    }

    public function getHashidAttribute()
    {
        return (new Hashid())->encode($this->id);
    }

    public function products_descriptions()
    {
        return $this->belongsTo(ProductsDescriptions::class);
    }

    public function scopeWhereActive($query)
    {
        return $query->where('published', 1);
    }
}
