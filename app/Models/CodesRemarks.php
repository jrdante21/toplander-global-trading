<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodesRemarks extends Model
{
    use HasFactory;
    public $table = 'codes_remarks';
    public $timestamps = false;
    
    public function codes()
    {
        return $this->hasMany(Codes::class, 'remarks_id');
    }
}
