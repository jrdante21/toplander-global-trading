<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembersNetworkUnilevel extends Model
{
    use HasFactory;
    
    const UNILEVELS = ['lvl0','lvl1','lvl2','lvl3','lvl4','lvl5','lvl6','lvl7','lvl8','lvl9','lvl10'];

    public $table = 'members_network_unilevel';
    protected $appends = ['total_profit'];

    public function getTotalProfitAttribute()
    {
        $total = 0;
        foreach ($this::UNILEVELS as $u) {
            $total += $this->$u;
        }

        return $total;
    }

    public function scopeJoinMembersNetwork($query)
    {
        return $query->join('members_network', function($join){
            $join->on('members_network_unilevel.members_network_id', '=', 'members_network.id');
        });
    }

    public function scopeTotalForMembers($query)
    {
        $levels = implode(' + ', $this::UNILEVELS);

        return $query->selectRaw("
            members_id,
            SUM(".$levels.") as total_profit_unilevel
        ")
        ->join('members_network', function($join){
            $join->on('members_network_unilevel.members_network_id', '=', 'members_network.id');
        });
    }

}
