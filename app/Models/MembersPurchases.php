<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembersPurchases extends Model
{
    use HasFactory;
    public $table = 'members_purchases';
    protected $appends = ['amount'];

    public function getAmountAttribute($value)
    {
        return $this->price_discounted * $this->quantity;
    }

    public function getConfirmedAtAttribute($value)
    {
        return date('M d, Y', strtotime($value));
    }

    public function getCreatedAtAttribute($value)
    {
        return date('M d, Y h:i A', strtotime($value));
    }

    public function network()
    {
        return $this->belongsTo(MembersNetwork::class, 'members_network_id');
    }

    public function product()
    {
        return $this->belongsTo(Products::class, 'products_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admins::class, 'admins_id');
    }

    public function scopeAddress($query)
    {
        $address = MembersNetwork::selectRaw("
            members_network.id as address_id,
            UPPER(province) as province,
            city,
            barangay
        ")
        ->join('members', 'members_network.members_id', '=', 'members.id')
        ->groupBy('address_id');

        return $query
        ->leftJoinSub($address, 'address', function ($join) {
            $join->on('members_purchases.members_network_id', '=', 'address.address_id');
        });
    }

    public function scopeJoinMembersNetwork($query)
    {
        return $query->join('members_network', function($join){
            $join->on('members_purchases.members_network_id', '=', 'members_network.id');
        });
    }

    public function scopeSumPurchases($query)
    {
        return $query->selectRaw("
            members_network_id,
            SUM(quantity * price) as amount_original,
            SUM(quantity * price_discounted) as amount_discounted
        ");
    }

    public function scopeWhereYearMonth($query, $date)
    {
        $date = date('Y-m', strtotime($date));
        return $query->whereRaw("DATE_FORMAT(confirmed_at, '%Y-%m') = ?", [$date]);
    }

    public function scopeRangeByMonth($query, $start='', $end='')
    {
        if (empty($start) && empty($end)) return $query;

        if (!empty($start)) $query->whereRaw("DATE_FORMAT(confirmed_at, '%Y-%m') >= ?", [$start]);
        if (!empty($end)) $query->whereRaw("DATE_FORMAT(confirmed_at, '%Y-%m') <= ?", [$end]);
        return $query;
    }

    public function scopeRangeByDate($query, $start='', $end='', $type='created_at')
    {
        if (empty($start) && empty($end)) return $query;
        return $query
        ->whereRaw("DATE(members_purchases.$type) >= ? && DATE(members_purchases.$type) <= ?", [
            date('Y-m-d', strtotime($start)),
            date('Y-m-d', strtotime($end))
        ]);
    }
}
