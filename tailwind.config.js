module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        '30px': '30px',
        '80px': '80px',
        '100px': '100px',
        '200px': '200px',
        '230px': '230px',
        '295px': '295px',
        '300px': '300px',
        '400px': '400px',
        '500px': '500px'
      },
      width: {
        '30px': '30px',
        '80px': '80px',
        '100px': '100px',
        '200px': '200px',
        '230px': '230px',
        '295px': '295px',
        '300px': '300px',
        '400px': '400px',
        '500px': '500px'
      },
      fontSize: {
        '10px': '10px',
        '11px': '11px',
        '12px': '12px',
        '13px': '13px',
        '14px': '14px',
        '15px': '15px',
        '16px': '16px',
        '18px': '18px',
        '20px': '20px',
        '24px': '24px',
        '28px': '28px',
        '30px': '30px',
        '32px': '32px',
        '35px': '35px',
        '36px': '36px',
        '38px': '38px',
        '48px': '48px',
        '55px': '55px',
        '58px': '58px',
        '64px': '64px'
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
