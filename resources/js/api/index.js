import axios from "axios"
import apiNotifier from "~/helpers/apiNotifier"

const API = axios.create({
    baseURL: '/api2'
})

API.interceptors.response.use(r => { return r }, error => {
    apiNotifier(error.response)
    return Promise.reject(error)
})

export default API