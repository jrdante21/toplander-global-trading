const regexpName = {pattern: "^[A-Za-z- ]+$", flag: 'i'}
const regexpMsg = "must be letters, spaces, or '-' only."

export default {
    fname: {
        presence: {
            allowEmpty: false,
            message: '^First Name is required.'
        },
        format: {
            ...regexpName,
            message: "^First Name "+regexpMsg
        },
        length: {
            minimum: 2,
            message: "^First Name must be at least 2 characters."
        },
    },
    mname: {
        presence: {
            allowEmpty: false, message:
            '^Middle Name is required'
        },
        format: {
            ...regexpName,
            message: "^Middle Name "+regexpMsg
        },
        length: {
            minimum: 2,
            message: "^Middle Name must be at least 2 characters."
        },
    },
    lname: {
        presence: {
            allowEmpty: false, message:
            '^Last Name is required'
        },
        format: {
            ...regexpName,
            message: "^Last Name "+regexpMsg
        },
        length: {
            minimum: 2,
            message: "^Last Name must be at least 2 characters."
        },
    },
    gender: {
        presence: {
            allowEmpty: false,
            message: 'is required.'
        }
    },
    bday: {
        presence: {
            allowEmpty: false,
            message: '^Birthdate is required.'
        }
    },
    contact_num: {
        presence: {
            allowEmpty: false,
            message: '^Contact Number is required.'
        },
        length: {
            minimum: 10,
            message: "^Contact Number must be at least 10 characters."
        },
        numericality: {
            message:"^Contact Number be numbers only."
        }
    },
    province: {
        presence: {
            allowEmpty: false,
            message: '^Province is required.'
        }
    },
    city: {
        presence: {
            allowEmpty: false, message: '^Municipality is required.'
        }
    },
    barangay: {
        presence: {
            allowEmpty: false,
            message: '^Barangay is required.'
        }
    },
}