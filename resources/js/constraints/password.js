export default {
    password: {
        presence: {
            allowEmpty: false,
            message: '^Password is required.'
        },
    },
    confirmPassword: {
        presence: {
            allowEmpty: false,
            message: '^Confirm password is required.'
        },
        equality: {
            attribute: 'password',
            message: '^Confirm password did not match to password.'
        }
    }
}