export default {
    presence: {
        allowEmpty: false,
        message: '^Username is required.'
    },
    format: {
        pattern:"[A-Za-z0-9]+",
        flag:'i',
        message: "^Username must be letters and numbers only."
    },
    length: {
        minimum: 5,
        message: "^Username must be at least 5 characters."
    },
}