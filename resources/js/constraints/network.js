export default {
    network_username: {
        presence: {
            allowEmpty: false,
            message: '^Username is required.'
        },
        format: {
            pattern:"[A-Za-z0-9]+",
            flag:'i',
            message: "^Username must be letters and numbers only."
        },
        length: {
            minimum: 5,
            message: "^Username must be at least 5 characters."
        },
    },
    sponsor: {
        presence: {
            allowEmpty: false,
            message: '^Sponsor is required.'
        }
    },
    placement: {
        presence: {
            allowEmpty: false,
            message: '^Placement is required.'
        }
    },
    position: {
        presence: {
            allowEmpty: false,
            message: '^Position is required.'
        }
    },
    code: {
        presence: {
            allowEmpty: false,
            message: '^Code is required.'
        },
        length: {
            minimum: 17,
            message: "^Code length is invalid."
        }
    }
}