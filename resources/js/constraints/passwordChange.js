export default {
    password: {
        presence: {
            allowEmpty: false,
            message: '^Password is required.'
        },
        length: {
            minimum: 5,
            message: "^Password must be at least 5 characters."
        },
    },
    password_confirmation: {
        presence: {
            allowEmpty: false,
            message: '^Confirm password is required.'
        },
        equality: {
            attribute: 'password',
            message: '^Confirm password did not match to password.'
        }
    },
    password_old: {
        presence: {
            allowEmpty: false,
            message: '^Old Password is required.'
        },
    }
}