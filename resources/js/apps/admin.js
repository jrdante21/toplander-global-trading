import { createApp } from 'vue'
import VueClipboard from 'vue3-clipboard'

import router from '~/apps/router/admin'
import store from '~/apps/store'

import NavigationTop from '~/components/Admin/Utilities/NavigationTop.vue'
import NavigationLeft from '~/components/Admin/Utilities/NavigationLeft.vue'

const app = createApp({
    components: { NavigationTop, NavigationLeft },
    provide: {
        isAdmin: true
    }
})

app.use(VueClipboard, {autoSetContainer: true, appendToBody: true})
app.use(router)
app.use(store)
app.mount('#app')