import { createApp } from 'vue'
import VueClipboard from 'vue3-clipboard'

import store from '~/apps/store'
import router from '~/apps/router/member'

import AlertError from '~/components/Utilities/Global/AlertError.vue'
import MembersHome from '~/components/Members/Home.vue'
import NavigationTop from '~/components/Members/Utilities/NavigationTop.vue'
const app = createApp({
    components: { AlertError, MembersHome, NavigationTop },
    provide: {
        isAdmin: false
    }
})

app.use(router)
app.use(store)
app.use(VueClipboard)
app.mount('#app')