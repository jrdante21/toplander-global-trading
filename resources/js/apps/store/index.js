import { createStore } from 'vuex'
export default createStore({
    state () {
        return {
            memberData: {},
            adminDashboard: {},
            adminData: {},
        }
    },
    mutations: {
        setMemberData (state, data) {
            state.memberData = data
        },
        setAdminDashboard (state, data) {
            state.adminDashboard = data
        },
        setAdminData (state, data) {
            state.adminData = data
        },
        decrementRequestPayout (state) {
            const count = state.adminDashboard.count_request_payouts - 1
            state.adminDashboard = Object.assign(state.adminDashboard, {
                count_request_payouts: count
            })
        }
    },
    getters: {
        isAdminSuper (state) {
            const { is_super } = state.adminData
            return (is_super >= 1) ? true : false
        },
        adminLevel (state) {
            const { level } = state.adminData
            return level
        }
    }
})