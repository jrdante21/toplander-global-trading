import { createApp } from 'vue'
import { createStore } from 'vuex'
import { createRouter, createWebHistory } from 'vue-router'

const app = createApp({})

import PageNotFound from '~/components/Utilities/PageNotFound.vue';
import Step1 from '~/components/MembersRegistration/Step1-ChooseAccount.vue'
import Step2New from '~/components/MembersRegistration/Step2-Profile.vue'
import Step2Old from '~/components/MembersRegistration/Step2-ProfileSearch.vue'
import Step3 from '~/components/MembersRegistration/Step3-ChoosePackage.vue'
import Step4 from '~/components/MembersRegistration/Step4-Network.vue'
import Step5 from '~/components/MembersRegistration/Step5-Review.vue'
const router = createRouter({
    history: createWebHistory(),
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/register/:pathMatch(.*)*', 
            name: 'not_found', 
            component: PageNotFound
        },{
            // Step 1
            path: '/register/',
            name: 'choose_account',
            component: Step1
        },{
            // Step 2 New Account
            path: '/register/profile',
            name: 'profile',
            component: Step2New
        },{
            // Step 2 Add Account
            path: '/register/search',
            name: 'search',
            component: Step2Old
        },{
            // Step 3
            path: '/register/package',
            name: 'package',
            component: Step3
        },{
            // Step 4
            path: '/register/network',
            name: 'network',
            component: Step4
        },{
            // Step 5
            path: '/register/review',
            name: 'review',
            component: Step5
        }
    ]
})

const store = createStore({
    state () {
        return {
            isNew: null,
            profile: {},
            network: {}
        }
    },
    mutations: {
        setChosenAccount (state, data) {
            state.isNew = data
        },
        setProfile (state, data) {
            state.profile = data
        },
        setProfileLogin (state, data) {
            state.profile = Object.assign(state.profile, data)
        },
        setNetwork (state, data) {
            state.network = Object.assign(state.network, data)
        },
        resetNetwork (state) {
            state.network = {}
        },
        resetState (state) {
            state.isNew = null
            state.profile = {}
            state.network = {}
        }
    }
})

app.use(store)
app.use(router)
app.mount('#app')