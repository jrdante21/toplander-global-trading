import { createRouter, createWebHistory } from 'vue-router'

import PageNotFound from '~/components/Utilities/PageNotFound.vue';
import MembersHomeMain from '~/components/Members/HomeMain.vue'
import MembersNetworks from '~/components/Members/Networks.vue'
import MembersSummaryProfits from '~/components/Members/SummaryProfits.vue'
import MembersSummaryPairSponsor from '~/components/Members/SummaryPairSponsor.vue'
import MembersPurchases from '~/components/Members/Purchases.vue'
import MembersPayouts from '~/components/Members/Payouts.vue'
import PayoutRequest from '~/components/Members/Forms/PayoutRequest.vue'

const router = createRouter({
    history: createWebHistory(),
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/distributor/dashboard/:pathMatch(.*)*', 
            name: 'not_found', 
            component: PageNotFound
        },{
            path: '/distributor/dashboard/',
            component: MembersHomeMain,
            name: 'dist_main',
        },{
            path: '/distributor/dashboard/networks',
            component: MembersNetworks,
            name: 'dist_networks',
        },{
            path: '/distributor/dashboard/profits/:type',
            component: MembersSummaryProfits,
            name: 'dist_profits',
            props: true
        },{
            path: '/distributor/dashboard/pairing_n_sponsoring',
            component: MembersSummaryPairSponsor,
            name: 'dist_pair_spon'
        },{
            path: '/distributor/dashboard/purchases',
            component: MembersPurchases,
            name: 'dist_purchases',
            props: true
        },{
            path: '/distributor/dashboard/payouts',
            component: MembersPayouts,
            name: 'dist_payouts',
            props: true
        },{
            path: '/distributor/forms/request_payout',
            name: 'request_payout',
            component: PayoutRequest
        }
    ]
})

export default router