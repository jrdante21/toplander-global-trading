import { createRouter, createWebHistory } from 'vue-router'

import PageNotFound from '~/components/Utilities/PageNotFound.vue';

// Distributors
import MembersHome from '~/components/Members/Home.vue'
import MembersHomeMain from '~/components/Members/HomeMain.vue'
import MembersNetworks from '~/components/Members/Networks.vue'
import MembersSummaryProfits from '~/components/Members/SummaryProfits.vue'
import MembersSummaryPairSponsor from '~/components/Members/SummaryPairSponsor.vue'
import MembersPurchases from '~/components/Members/Purchases.vue'
import MembersPayouts from '~/components/Members/Payouts.vue'

// Admin
import AdminHome from '~/components/Admin/Home.vue'
import AdminMembers from '~/components/Admin/Members.vue'
import AdminCodes from '~/components/Admin/Codes.vue'
import AdminPayouts from '~/components/Admin/Payouts.vue'
import AdminPayoutsMain from '~/components/Admin/PayoutsMain.vue'
import AdminPayoutsUnpaids from '~/components/Admin/PayoutsUnpaids.vue'
import AdminPurchases from '~/components/Admin/Purchases.vue'
import AdminProducts from '~/components/Admin/Products.vue'
import AdminProductsDescriptions from '~/components/Admin/ProductsDescriptions.vue'
import AdminProductsList from '~/components/Admin/ProductsList.vue'
import Admins from '~/components/Admin/Admins.vue'

import AdminForm from '~/components/Admin/Form.vue'
import AdminForm2 from '~/components/Admin/Form2.vue'
import AddPurchase from '~/components/Admin/Forms/AddPurchase.vue'
import AddPayout from '~/components/Admin/Forms/AddPayout.vue'
import AddMember from '~/components/Admin/Forms/AddMember.vue'
import ModifyMembersNetwork from '~/components/Admin/Forms/ModifyMembersNetwork.vue'

const router = createRouter({
    history: createWebHistory(),
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/topadminportal/panel/:pathMatch(.*)*', 
            name: 'not_found', 
            component: PageNotFound
        },{
            path: '/topadminportal/panel/',
            alias: '/topadminportal/panel/home',
            name: 'home',
            component: AdminHome,
        },{
            path: '/topadminportal/panel/distributors',
            name: 'distributors',
            component: AdminMembers,
        },{
            path: '/topadminportal/panel/codes',
            name: 'codes',
            component: AdminCodes,
        },{
            path: '/topadminportal/panel/payouts',
            component: AdminPayouts,
            children: [
                {
                    path: '',
                    name: 'payouts_main',
                    component: AdminPayoutsMain
                },{
                    path: 'unpaids',
                    name: 'payouts_unpaids',
                    component: AdminPayoutsUnpaids
                }
            ]
        },{
            path: '/topadminportal/panel/purchases',
            name: 'purchases',
            component: AdminPurchases,
        },{
            path: '/topadminportal/panel/products',
            component: AdminProducts,
            children: [
                {
                    path: '',
                    name: 'products_descriptions',
                    component: AdminProductsDescriptions
                },{
                    path: 'list',
                    name: 'products_list',
                    component: AdminProductsList
                }
            ]
        },{
            path: '/topadminportal/panel/admins',
            name: 'admins',
            component: Admins,
        },{
            path: '/topadminportal/panel/distributor/:id',
            component: MembersHome,
            props: true,
            children: [
                {
                    path: '',
                    component: MembersHomeMain,
                    name: 'dist_main',
                },{
                    path: 'networks',
                    component: MembersNetworks,
                    name: 'dist_networks',
                },{
                    path: 'profits/:type',
                    component: MembersSummaryProfits,
                    name: 'dist_profits',
                    props: true
                },{
                    path: 'pairing_n_sponsoring',
                    component: MembersSummaryPairSponsor,
                    name: 'dist_pair_spon'
                },{
                    path: 'purchases',
                    component: MembersPurchases,
                    name: 'dist_purchases',
                    props: true
                },{
                    path: 'payouts',
                    component: MembersPayouts,
                    name: 'dist_payouts',
                    props: true
                }
            ]
        },{
            path: '/topadminportal/forms',
            component: AdminForm,
            children: [
                {
                    path: 'add_purchase',
                    name: 'add_purchase',
                    component: AddPurchase
                },{
                    path: 'add_payout',
                    name: 'add_payout',
                    component: AddPayout
                },{
                    path: 'modify_network',
                    name: 'modify_network',
                    component: ModifyMembersNetwork
                }
            ]
        },{
            path: '/topadminportal/forms2',
            component: AdminForm2,
            children: [
                {
                    path: 'add_member',
                    name: 'add_member',
                    component: AddMember
                }
            ]
        }
    ]
})

export default router