import { createApp } from 'vue'
import GenealogyBinary from '~/components/Members/Genealogy.vue'

const app = createApp({
    components: { GenealogyBinary }
})
app.mount('#app')