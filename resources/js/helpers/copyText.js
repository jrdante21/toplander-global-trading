const sleep = m => new Promise(r => setTimeout(r, m))
export default async (text) => {
    const listener = function(ev) {
        ev.preventDefault();
        ev.clipboardData.setData('text/plain', text)
    }

    document.addEventListener('copy', listener)
    document.execCommand('copy')
    document.removeEventListener('copy', listener)

    var tooltip = document.createElement("div")
    tooltip.className = "fixed z-50 top-2 right-2 p-3 rounded bg-black text-white text-xl animate__animated"
    tooltip.appendChild(document.createTextNode(`${text} copied!`))
    document.body.append(tooltip)
    tooltip.classList.add("animate__fadeInDown")

    await sleep(3000)
    tooltip.classList.add("animate__fadeOutUp")
    await sleep(2000)
    tooltip.remove()
}