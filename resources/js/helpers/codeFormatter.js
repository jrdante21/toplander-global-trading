export default (text='') => {
    if (!text) return ''

    var code = text.replace(/[^0-9a-z]/gi,'')
    var codes = []
    for (let index = 0; index <= 10; index+= 5) {
        const str = code.substring(index, (index + 5))
        if (str != '') codes.push(str)
    }
    code = codes.join('-')
    return code.toUpperCase()
}