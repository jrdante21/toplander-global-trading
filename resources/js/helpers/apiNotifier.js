const sleep = m => new Promise(r => setTimeout(r, m))
export default (response={}) => {
    const { status, data } = response

    var result = {}
    result.title = (data.message) ? data.message : data
    result.details = ""
    result.message = ""

    switch (status) {
        case 404:
            result.title = "Request Not Found."
            result.message = "Undefined request has been sent."
            break;

        case 422:
            result.message = "Please check and complete the fields with errors."
            break;
        
        case 401:
            result.message = "Reload this page and log in again."
            break;
        
        case 403:
            result.message = "You are not allowed to do this action."
            break;

        case 500:
            result.title = "Something went wrong."
            result.message = "Please try again later."
            result.details = JSON.stringify(data).substring(0, 200)
            break;
    }

    const details = (result.details) ? `<div class="text-xs text-gray-600 mt-2 whitespace-normal break-all">${result.details}...</div>` : ''
    const container = document.createElement('div')
    container.className = 'animate__animated animate__bounceIn fixed top-0 inset-x-0 z-50'
    container.innerHTML = `
        <div class="relative shadow-lg bg-white p-4 lg:w-1/3 md:w-3/4 md:rounded md:mx-auto my-10 border-gray-300 border-solid border">
            <div class="flex gap-4">
                <div class="text-48px text-red-500 fas fa-times-circle"></div>
                <div class="flex-1">
                    <div class="text-xl font-medium text-red-500">${result.title}</div>
                    <div>${result.message}</div>
                    ${details}
                    <div class="text-xs text-gray-600 italic text-right">Click to close</div>
                </div>
            </div>
        </div>
    `

    const input = document.createElement('input')
    input.className = "absolute w-0 -left-px"
    container.append(input)
    document.body.append(container)

    input.focus()
    container.addEventListener('focusout', async () => {
        container.classList.add("animate__fadeOutUp")
        await sleep(2000)
        container.remove()
    })
}