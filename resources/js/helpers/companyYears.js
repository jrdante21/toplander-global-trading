function companyYears() {
    var years = []
    const yearNow = new Date().getFullYear()
    for (let index = 2018; index <= yearNow; index++) {
        years.push(index)
    }

    return years.reverse()
}

export default companyYears()