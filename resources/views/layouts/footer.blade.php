<footer class="bg-gray-800 px-3 py-3 md:py-10 text-gray-300 mt-20">
    <div class="container mx-auto">
        <div class="flex flex-col lg:flex-row gap-x-5 gap-y-10">
            <div class="flex-1">
                <div class="text-gray-400 uppercase font-medium mb-4">Offices</div>
                <div class="flex flex-col gap-4">
                    @foreach ($details['offices'] as $o)
                        <div>
                            <div class="font-medium">{{ $o['title'] }}</div>
                            <ul class="list-none list-inside text-sm">
                                <li class="pl-2"><i class="fas fa-map-marker-alt mr-2"></i> {{ $o['location'] }}</li>
                                <li class="pl-2"><i class="fas fa-phone mr-2"></i> {{ $o['contact_num'] }}</li>
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="flex-1">
                <div class="text-gray-400 uppercase font-medium mb-4">About Us</div>
                <div class="text-sm mb-4">{{ implode(' ', $details['about_us']) }}</div>
                <div class="flex flex-col md:flex-row gap-x-4 gap-y-2">
                    <a href="/credential/privacy-policy">Privacy Policy</a>
                    <a href="/credential/terms-and-conditions">Terms & Conditions</a>
                    <a href="/credential/disclaimer">Disclaimer</a>
                    <span>© 2018. All Rights Reserved</span>
                </div>
            </div>
        </div>
    </div>
</footer>