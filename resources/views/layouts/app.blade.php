<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="icon" href="/images/logo.png">
    {!! SEO::generate() !!}
    <title>@yield('title')</title>
    @stack('scripts')
</head>
<style>
    .main-banner { height: calc(100vh - 72px); }
    @media only screen and (max-width: 770px) {
        .main-banner { height: auto; }
    }
</style>
<body class="bg-blue-50">
    <div>
        @yield('content')
    </div>
</body>
</html>