@php
    $menu = [
        'home' => ['route' => '/', 'title' => 'Home'],
        'products' => ['route' => '/products', 'title' => 'Products'],
        'marketing' => ['route' => '/marketing', 'title' => 'Marketing'],
        'about_us' => ['route' => '/about_us', 'title' => 'About Us'],
        'register' => ['route' => '/register', 'title' => 'Register'],
    ];
@endphp
<div x-data="{ openNavOnMobile:false }" class="sticky top-0 bg-white shadow-md p-3 z-40">
    <div class="container mx-auto">
        <div class="flex items-center gap-3">
            <div class="block md:hidden">
                <button class="button" x-on:click="openNavOnMobile = true"><i class="fas fa-bars"></i></button>
            </div>
            <div class="flex-1">
                <a class="flex gap-2 items-center" href="/">
                    <div>
                        <img src="/images/logo.png" class="w-10" alt="logo">
                    </div>
                    <div class="flex-1 font-medium text-lg hidden lg:block">{{ $details['title'] }}</div>
                </a>
            </div>
            <div class="flex-1 hidden md:block">
                <div class="menu justify-end gap-4">
                    @foreach ($menu as $k => $m)
                    @php
                        $active = (request()->routeIs('site.'.$k)) ? 'active' : '';
                    @endphp
                        <a href="{{ $m['route'] }}" class="item {{ $active }}">{{ $m['title'] }}</a>
                    @endforeach
                </div>
            </div>
            @if (!request()->routeIs('member.login'))
                <div>
                    @if (auth()->guard('member')->check())
                        <a href="/distributor/dashboard" class="button text-sm">Dashboard</a>
                    @else
                        <a href="/distributor" class="button text-sm">Log In</a>
                    @endif
                </div>
            @endif
        </div>
    </div>

    <div class="fixed inset-0 z-50 bg-black bg-opacity-50 md:hidden" x-show="openNavOnMobile" x-transition:enter="animate__animated animate__fadeInLeft" x-transition:leave="animate__animated animate__fadeOutLeft">
        <div class="absolute inset-0 z-10" x-on:click="openNavOnMobile = false"></div>
        <div class="bg-white shadow-md h-screen relative w-4/5 z-20 overflow-auto">
            <div class="flex items-center gap-2 p-4">
                <div><img src="/images/logo.png" class="w-10" alt="logo"></div>
                <div class="flex-1">
                    <div class="font-medium">{{ $details['title'] }}</div>
                </div>
                <div>
                    <i class="text-xl link fas fa-times" x-on:click="openNavOnMobile = false"></i>
                </div>
            </div>
            <div class="menu-col big">
                @foreach ($menu as $k => $m)
                @php
                    $active = (request()->routeIs('site.'.$k)) ? 'active' : '';
                @endphp
                    <a href="{{ $m['route'] }}" class="item {{ $active }}">{{ $m['title'] }}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
@endpush