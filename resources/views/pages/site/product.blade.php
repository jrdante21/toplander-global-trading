@extends('layouts.app')
@section('title', $product->name)
@section('content')
<div>
    @include('layouts.navbar')
    <div class="container px-3 xl:px-0 mx-auto my-10">
        <div class="text-red-700 text-xl md:text-4xl mb-5 font-bold">{{ $product->name }}</div>
        <div class="flex flex-col lg:flex-row gap-10">
            <div class="flex-1">
                <div class="bg-white rounded shadow-md">
                    <div class="bg-gradient-to-tl from-green-400 to-green-600 h-500px rounded-t p-4">
                        <div class="h-full w-full bg-contain bg-center bg-no-repeat" style="background-image: url({{ $product->image }})"></div>
                    </div>
                    <div class="p-4">
                        <div class="text-lg font-medium">
                            <span class="text-gray-600">SRP :</span> {{ number_format($product->price) }}
                        </div>
                        @if (!empty($product->products_descriptions))
                            <div class="text-gray-600 mt-5 text-lg">
                                {!! $product->products_descriptions->html !!}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="flex-1 lg:flex-initial lg:w-1/3">
                <div class="uppercase font-bold text-gray-600 mb-5">Products</div>
                <div class="flex flex-col gap-5">
                    @foreach (product_categories() as $p)
                    @if (!empty($products[$p]))
                        <div>
                            <div class="font-medium text-lg mb-3 capitalize">{{ $p }}</div>
                            <div class="flex flex-col gap-2 pl-3">
                                @foreach ($products[$p] as $i)
                                    <a href="/products/{{ $i->hashid }}" class="link">{{ $i->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection