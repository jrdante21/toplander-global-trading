@extends('layouts.app')
@section('title', 'Products')
@section('content')
<div>
    @include('layouts.navbar')
    <div class="main-banner bg-red-gradient bg-cover bg-no-repeat">
        <div class="h-auto lg:h-full container py-10 px-3 xl:px-0 mx-auto flex flex-col lg:items-center lg:flex-row gap-x-10 gap-y-5">
            <div class="flex-1">
                <div class="text-white font-extrabold text-3xl md:text-7xl mb-4">{{ $details['introduction']['products']['header'] }}</div>
                <div class="text-white text-lg md:text-xl mb-10">{{ implode(' ', $details['introduction']['products']['description']) }}</div>
            </div>
            <div class="flex-1">
                <iframe class="w-full h-200px xl:h-500px lg:h-300px md:h-500px" src="https://drive.google.com/file/d/1lQ7UpYgON75B9BwuWE0GaIETBluZKFBO/preview" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
            </div>
        </div>
    </div>
    <div class="container px-3 xl:px-0 mx-auto my-10">
        <div class="flex flex-col gap-10">
            @foreach (product_categories() as $p)
            @if (!empty($products[$p]))
                <div>
                    <div class="font-extrabold text-xl md:text-2xl mb-5 capitalize">{{ $p }}</div>
                    <div class="grid grid-cols-1 md:grid-cols-3 xl:grid-cols-5 lg:grid-cols-4 gap-5">
                        @foreach ($products[$p] as $i)
                            <div class="bg-white rounded shadow-md">
                                <div class="bg-gradient-to-tl from-green-400 to-green-600 h-300px rounded-t p-4">
                                    <div class="h-full w-full bg-contain bg-center bg-no-repeat" style="background-image: url({{ $i->image }})"></div>
                                </div>
                                <div class="p-4">
                                    <div class="text-red-700 text-lg font-bold">{{ $i->name }}</div>
                                    <div class="text-lg font-medium">
                                        <span class="text-gray-600">SRP :</span> {{ number_format($i->price) }}
                                    </div>
                                    <a href="/products/{{ $i->hashid }}" class="button block text-center w-full mt-5">View Product</a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            @endforeach
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection