@extends('layouts.app')
@section('title', 'About Us')
@section('content')
<div>
    @include('layouts.navbar')
    <div class="main-banner bg-red-gradient bg-cover bg-no-repeat" style="background-image: url(https://res.cloudinary.com/dthkytjyl/image/upload/v1637892961/site/banner-about-us_rvu6gj.jpg)">
        <div class="h-auto lg:h-full container py-10 px-3 xl:px-0 mx-auto flex flex-col lg:items-center lg:flex-row gap-x-10 gap-y-5">
            <div class="flex-1">
                <div class="text-white font-extrabold text-3xl md:text-7xl mb-4">About Our Company</div>
                <div class="text-white text-sm md:text-lg">
                    @foreach ($details['about_us'] as $a)
                        <p>{{ $a }}</p>
                    @endforeach
                </div>
            </div>
            <div class="flex-1">
                <iframe class="w-full h-200px xl:h-500px lg:h-300px md:h-500px" src="https://drive.google.com/file/d/1Pt6JKrx5SM7JWSdMmbfz3gK-irNtuDwe/preview" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
            </div>
        </div>
    </div>
    <div class="container px-3 xl:px-0 mx-auto mt-20">
        <div class="flex flex-col md:flex-row gap-10">
            @foreach ($details['management'] as $m)
                <div class="flex-1 text-center">
                    <div class="shadow-md inline-block h-200px md:h-300px w-200px md:w-300px bg-center bg-no-repeat bg-cover rounded-full" style="background-image: url({{ $m['image'] }})"></div>
                    <div class="mt-5">
                        <div class="text-3xl font-medium mb-2">{{ $m['name'] }}</div>
                        <div class="text-xl text-gray-600">{{ $m['title'] }}</div>
                    </div>
                </div>
            @endforeach
        </div>
        @php
            $about = [
                "Our Vision" => $details['vision'],
                "Our Mission" => $details['mission'],
                "Core Values" => $details['core_values'],
            ];
        @endphp
        <div class="flex flex-col md:flex-row md:flex-stretch gap-10 mt-20 text-center">
            @foreach ($about as $ak => $a)
                <div class="flex-1 bg-white rounded shadow-md p-4">
                    <div class="font-extrabold text-xl md:text-4xl mb-5 text-center">{{ $ak }}</div>
                    <div class="text-normal lg:text-xl">{{ implode(' ', $a) }}</div>
                </div>
            @endforeach
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection