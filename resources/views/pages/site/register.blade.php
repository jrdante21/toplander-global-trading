@extends('layouts.app')
@section('title', 'Register')
@section('content')
<div id="app">
    @include('layouts.navbar')
    <div class="px-2 mx-auto w-full xl:w-1/2 my-20">
        <router-view></router-view>
    </div>
</div>

<script src="{{ mix('js/register.js') }}"></script>
@endsection