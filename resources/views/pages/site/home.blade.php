@extends('layouts.app')
@section('title', 'Home')
@section('content')
<div>
    @include('layouts.navbar')
    <div class="main-banner bg-red-gradient">
        <div class="h-auto lg:h-full container py-10 px-3 xl:px-0 mx-auto flex flex-col lg:items-center lg:flex-row gap-x-10 gap-y-5">
            <div class="flex-1">
                <div class="text-white font-extrabold text-3xl md:text-7xl mb-4">{{ $details['introduction']['main']['header'] }}</div>
                <div class="text-white text-lg md:text-xl mb-10">{{ implode(' ', $details['introduction']['main']['description']) }}</div>
                <a href="/marketing" class="button text-xl">Learn More <i class="ml-5 fas fa-arrow-right"></i></a>
            </div>
            <div class="flex-1">
                <iframe class="w-full h-200px xl:h-500px lg:h-300px md:h-500px" src="https://www.youtube.com/embed/_5o2TUxSuJs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
    <div class="container px-3 xl:px-0 mx-auto">
        <div class="text-red-700 text-xl md:text-4xl mt-20 mb-5 font-bold text-center">Our Advocacies</div>
        <div class="flex flex-col lg:items-stretch lg:flex-row gap-x-10 gap-y-5">
            @foreach ($details['advocacies'] as $d)
                <div class="bg-white rounded shadow-md p-4 md:p-10 flex-1">
                    <div class="text-center mb-10">
                        <i class="text-9xl text-red-700 {{ $d['icon'] }}"></i>
                    </div>
                    <div class="text-xl text-center font-medium">{{ $d['summary'] }}</div>
                </div>
            @endforeach
        </div>

        <div class="text-red-700 text-xl md:text-4xl mt-20 mb-5 font-bold text-center">Our Best Products</div>
        <div class="flex flex-col gap-10">
            @foreach ($products as $p)
            @php
                $flex = ($loop->odd) ? 'flex-row' : 'flex-row-reverse';
            @endphp
                <div class="flex flex-col md:{{ $flex }} md:items-center gap-y-4">
                    <div class="flex-1 lg:flex-initial lg:w-1/3 bg-gradient-to-tl from-green-400 to-green-600 p-4 rounded shadow-md">
                        <div class="h-300px md:h-400px w-full bg-contain bg-center bg-no-repeat" style="background-image: url({{ $p->image }})"></div>
                    </div>
                    <div class="flex-1">
                        <div class="p-4 lg:p-20">
                            <div class="text-2xl font-medium mb-5">{{ $p->name }}</div>
                            <div class="text-gray-600 text-normal lg:text-lg">{!! $p->products_descriptions->html !!}</div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="text-center">
                <a class="button-blue text-xl" href="/products">View More <i class="ml-5 fas fa-arrow-right"></i></a>
            </div>
        </div>

        <div class="text-red-700 text-xl md:text-4xl mt-20 mb-5 font-bold text-center">Awards</div>
        <div class="flex flex-col lg:items-stretch lg:flex-row gap-x-10 gap-y-5">
            @foreach ($details['awards'] as $a)
                <div class="flex-1">
                    <img src="{{ $a['image'] }}" class="w-full mb-5" alt="award">
                    <div class="text-center">
                        <div class="mb-3 font-medium">
                            <div class="text-2xl">{{ $a['name'] }}</div>
                            <div class="text-xl">{{ $a['title'] }}</div>
                        </div>
                        <div class="px-0 md:px-10">{{ $a['description'] }}</div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection