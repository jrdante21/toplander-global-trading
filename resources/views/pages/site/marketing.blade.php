@extends('layouts.app')
@section('title', 'Marketing')
@section('content')
<div>
    @include('layouts.navbar')
    <div class="container px-3 xl:px-0 mx-auto my-10">
        <div class="text-red-700 text-xl md:text-4xl mb-5 font-bold text-center">Packages We Offer</div>
        <div class="flex flex-col lg:flex-row lg:flex-stretch gap-10">
            @foreach ($details['packages'] as $p)
                <div class="bg-white rounded shadow-md p-4 md:p-10 text-center flex-1">
                    <div class="text-red-700 text-2xl font-bold">{{ $p['title'] }}</div>
                    <div class="text-gray-600 mt-5 text-lg">{{ $p['description'] }}</div>
                </div>
            @endforeach
        </div>

        <div class="text-red-700 text-xl md:text-4xl mt-20 mb-5 font-bold text-center">Member's Benefits</div>
        <div class="flex flex-col gap-10">
            @foreach ($details['members_benefits'] as $m)
                <div>
                    <div class="font-extrabold text-xl md:text-2xl capitalize">{{ $m['name'] }}</div>
                    <div class="text-gray-600 text-lg md:text-xl">
                        @if (!empty($m['description']))
                            <p class="mt-3">{!! implode('<br>', $m['description']) !!}</p>
                        @endif
                        @if (!empty($m['benefits']))
                            <div class="text-red-700 text-lg font-bold mb-1 mt-3">Benefits</div>
                            <ul class="list-disc list-inside">
                                @foreach ($m['benefits'] as $b)
                                    <li>{{ $b }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @if (!empty($m['requirements']))
                            <div class="text-red-700 text-lg font-bold mb-1 mt-3">Requirements</div>
                            <ul class="list-disc list-inside">
                                @foreach ($m['requirements'] as $b)
                                    <li>{{ $b }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection