@extends('layouts.app')
@section('title', $name)
@section('content')
<div>
    @include('layouts.navbar')
    <div class="container px-3 xl:px-0 mx-auto mt-20">
        <div class="text-red-700 text-xl md:text-4xl mb-5 font-bold text-center">{{ $name }}</div>
        @include('pages.site.credentials.'.$slug)
    </div>
    @include('layouts.footer')
</div>
@endsection