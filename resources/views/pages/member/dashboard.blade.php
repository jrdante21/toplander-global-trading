@extends('layouts.app')
@section('title', $member->halfname)
@section('content')
<div id="app">
    <div class="sticky top-0 bg-red-gradient p-3 z-40">
        <navigation-top></navigation-top>
    </div>
    <div class="container mx-auto px-3 lg:px-0">
        <members-home><members-home/>
    </div>
    <br><br><br>
</div>
<script src="{{ mix('js/member.js') }}"></script>
@endsection