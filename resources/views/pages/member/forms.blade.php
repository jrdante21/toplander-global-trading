@extends('layouts.app')
@section('title', $member->halfname)

@section('content')
<div id="app">
    <div class="sticky top-0 bg-red-gradient p-3 z-40">
        <div class="w-full mx-auto xl:w-1/2 lg:w-2/3">
            <div class="flex items-center gap-3">
                <div class="flex-1">
                    <div class="text-lg font-medium text-white">{{ $member->halfname }}</div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-full mx-auto xl:w-1/2 lg:w-2/3 p-3">
        <router-view></router-view>
    </div>
</div>
<script src="{{ mix('js/member.js') }}"></script>
@endsection