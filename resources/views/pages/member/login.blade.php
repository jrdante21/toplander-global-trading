@extends('layouts.app')
@section('title', 'Log in | Distributor')
@section('content')
<div>
    @include('layouts.navbar')
    <div class="w-full xl:w-3/5 lg:w-2/3 mx-auto mt-10 md:mt-20 mb-40 px-5">
        <div class="mb-5 text-xl md:text-3xl font-bold text-red-700">Distributor's Log In</div>
        <div class="flex flex-col-reverse gap-10 md:flex-row">
            <div class="flex-1">
                <div class="flex flex-col gap-4">
                    <div>
                        <h2 class="text-xl font-medium mb-2">Mission</h2>
                        <p class="text-lg text-gray-600">{{ implode(' ', $details['mission']) }}</p>
                    </div>
                    <div>
                        <h2 class="text-xl font-medium mb-2">Vision</h2>
                        <p class="text-lg text-gray-600">{{ implode(' ', $details['vision']) }}</p>
                    </div>
                </div>
            </div>
            <div class="flex-1">
                <div class="rounded p-4 shadow-md bg-white">
                    <form action="/distributor/login" method="POST">
                        @csrf
                        <div class="flex flex-col gap-4">
                            <div>
                                <label>Username</label>
                                <input type="text" value="{{ old('username') }}" name="username" class="input" required>
                            </div>
                            <div>
                                <label>Password</label>
                                <input type="password" name="password" class="input" required>
                            </div>
                            <div>
                                <button class="button-blue w-full">Log In</button>
                            </div>
                        </div>
                    </form>

                    @php $errors = (is_array($errors)) ? $errors : $errors->all(); @endphp
                    @if (!empty($errors))
                        <div class="mt-4 font-semibold text-red-700">
                            <ul class="pl-4 list-disc">
                                @foreach ($errors as $e)
                                    <li>{{ $e }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="mt-5">
                        No account yet? <a href="/register" class="text-blue-500 hover:underline">Register now.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</div>
@endsection