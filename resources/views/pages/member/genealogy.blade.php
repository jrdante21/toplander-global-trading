@extends('layouts.app')
@section('title', 'Genealogy')

@section('content')
<div id="app">
    <genealogy-binary id="{{ $id }}"></genealogy-binary>
</div>

<script src="{{ mix('js/genealogy.js') }}"></script>
@endsection