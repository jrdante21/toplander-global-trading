@extends('layouts.app')
@section('title', 'Topadminportal')

@section('content')
<div id="app">
    <div class="sticky top-0 bg-red-gradient p-3 z-40">
        <navigation-top></navigation-top>
    </div>

    <div class="fixed inset-y-auto left-0 bg-white shadow-md w-60 hidden lg:block" style="height: calc(100vh - 42px)">
        <navigation-left></navigation-left>
    </div>

    <div class="ml-0 lg:ml-60 px-2 md:px-5">
        <router-view></router-view>
    </div>
    <br><br><br>
</div>

<script src="{{ mix('js/admin.js') }}"></script>
@endsection