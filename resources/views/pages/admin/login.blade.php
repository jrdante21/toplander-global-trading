@extends('layouts.app')
@section('title', 'Log In | Topadminportal')

@section('content')
<div>
    <div class="mt-10 px-3 mx-auto w-full xl:w-1/4 lg:w-1/3 md:w-1/2">
        <div class="text-center mb-5">
            <img src="/images/logo.png" class="inline-block w-20 lg:w-40 md:w-32 mb-3" alt="logo">
            <div class="text-3xl text-red-700 font-medium">
                Topadminportal
            </div>
        </div>
        <div class="p-4 bg-white rounded shadow-md">
            <form action="/topadminportal/login" method="POST">
                @csrf
                <div class="flex flex-col gap-4">
                    <div>
                        <label>Username</label>
                        <input type="text" value="{{ old('username') }}" name="username" class="input" required>
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" name="password" class="input" required>
                    </div>
                    <div>
                        <button class="button-blue w-full">Log In</button>
                    </div>
                </div>
            </form>
            @php $errors = (is_array($errors)) ? $errors : $errors->all(); @endphp
            @if (!empty($errors))
                <div class="mt-4 font-semibold text-red-700">
                    <ul class="pl-4 list-disc">
                        @foreach ($errors as $e)
                            <li>{{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection