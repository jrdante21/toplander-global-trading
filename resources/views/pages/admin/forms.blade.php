@extends('layouts.app')
@section('title', 'Topadminportal')

@section('content')
<div id="app">
    <router-view></router-view>
    <br><br><br>
</div>

<script src="{{ mix('js/admin.js') }}"></script>
@endsection