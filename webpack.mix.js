const { version } = require('laravel-mix');
const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.webpackConfig({
    output: {
        chunkFilename: '[name].js?id=[chunkhash]',
    },
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
        '~': __dirname + '/resources/js'
        }
    }
});

// Javascript
mix
    .js('resources/js/apps/admin.js', 'public/js')
    .js('resources/js/apps/genealogy.js', 'public/js')
    .js('resources/js/apps/member.js', 'public/js')
    .js('resources/js/apps/site.js', 'public/js')
    .js('resources/js/apps/register.js', 'public/js')
    .vue(); // Vue compiler

// CSS and Sass
mix
    .sass('resources/sass/app.sass', 'public/css')
    .postCss('resources/css/app.css', 'public/css')
    .options({
        processCssUrls: false,
        postCss: [tailwindcss('./tailwind.config.js')],
    });

// Other
mix
    .copyDirectory('resources/assets/fontawesome/webfonts', 'public/webfonts')
    .copyDirectory('resources/images', 'public/images')
    .sourceMaps();

mix.version();