<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UpdateDBController as UpdateDB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/update_network_directs', [UpdateDB::class, 'network_directs']);
// Route::get('/update_and_fix_address', [UpdateDB::class, 'fix_address']);