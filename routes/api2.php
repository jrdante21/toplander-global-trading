<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\MembersController as Members;
use App\Http\Controllers\API\MembersNetworkController as Networks;
use App\Http\Controllers\API\SummaryController as Summary;
use App\Http\Controllers\API\GenealogyController as Genealogy;
use App\Http\Controllers\API\PurchasesController as Purchases;
use App\Http\Controllers\API\PayoutsController as Payouts;
use App\Http\Controllers\API\CodesController as Codes;
use App\Http\Controllers\API\RegistrationController as Register;
use App\Http\Controllers\API\ProductsController as Products;
use App\Http\Controllers\API\AdminController as Admin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| Everyone API
|--------------------------------------------------------------------------
*/
Route::get('/register_search_account', [Register::class, 'searchAccount']);
Route::post('/register_check_profile', [Register::class, 'checkProfile']);
Route::post('/register_check_login', [Register::class, 'checkLogin']);
Route::post('/register_check_network', [Register::class, 'checkNetwork']);
Route::post('/register_account', [Register::class, 'register']);
Route::post('/levelup_networking_account', [Register::class, 'levelUp']);

/*
|--------------------------------------------------------------------------
| Admin and Member API
|--------------------------------------------------------------------------
*/
Route::middleware('auth-api:member')->group(function () {
    Route::get('/member', [Members::class, 'member']);
    Route::get('/member_benefits', [Members::class, 'memberBenefits']);
    Route::get('/member_downline_binary', [Networks::class, 'downlinesPairing']);
    Route::get('/member_genealogy', [Networks::class, 'genealogy']);
    Route::get('/member_summary_profits', [Summary::class, 'profits']);
    Route::get('/member_summary_pairings_sponsoring', [Summary::class, 'pairingsSponsorings']);
    Route::get('/member_purchases', [Purchases::class, 'member']);
    Route::get('/member_payouts', [Payouts::class, 'member']);
    Route::post('/member_change_password', [Members::class, 'changePassword']);
    Route::post('/request_payout', [Payouts::class, 'request']);
});

/*
|--------------------------------------------------------------------------
| Admin API
|--------------------------------------------------------------------------
*/
// Level 1
Route::middleware('auth-api:admin1')->group(function () {
    Route::get('/admin_dashboard_data', [Summary::class, 'adminDashboard']);
    Route::get('/members_search', [Members::class, 'search']);
    Route::get('/members_list', [Members::class, 'list']);
    Route::get('/payouts_confirmed', [Payouts::class, 'confirmed']);
    Route::get('/payouts_requested', [Payouts::class, 'requested']);
    Route::get('/payouts_unpaids', [Payouts::class, 'unpaids']);
    Route::get('/products_all', [Products::class, 'all']);
    Route::get('/products_list', [Products::class, 'list']);
    Route::get('/products_descriptions', [Products::class, 'descriptions']);
    Route::get('/products_images', [Products::class, 'images']);
    Route::post('/admin_edit_username', [Admin::class, 'editUsername']);
    Route::post('/admin_change_password', [Admin::class, 'changePassword']);
    Route::get('/purchase_list', [Purchases::class, 'list']);
});

// Level 2
Route::middleware('auth-api:admin2')->group(function () {
    Route::post('/member_edit/{type}', [Members::class, 'edit']);
    Route::post('/network_edit_username', [Networks::class, 'editUsername']);
    Route::post('/purchase_add', [Purchases::class, 'add']);
});

// Level 3
Route::middleware('auth-api:admin3')->group(function () {
    Route::get('/codes', [Codes::class, 'all']);
    Route::post('/codes_generate', [Codes::class, 'generate']);
    Route::post('/payout_confirm', [Payouts::class, 'confirm']);
    Route::post('/payout_add', [Payouts::class, 'add']);
});

// Super Admin
Route::middleware('auth-api:super')->group(function () {
    Route::get('/admins', [Admin::class, 'all']);
    Route::post('/admin_create_update', [Admin::class, 'createOrUpdate']);
    Route::post('/admin_reset_password', [Admin::class, 'resetPassword']);
    Route::post('/admin_deactivate_activate', [Admin::class, 'deactivateOrActivate']);

    Route::post('/purchase_delete', [Purchases::class, 'delete']);
    Route::post('/payout_edit', [Payouts::class, 'edit']);
    Route::post('/payout_delete', [Payouts::class, 'delete']);
    Route::post('/product_create_update', [Products::class, 'createUpdateProduct']);
    Route::post('/product_publish', [Products::class, 'publishOrUnpublish']);
    Route::post('/product_description_create_update', [Products::class, 'createUpdateDescription']);
    Route::post('/product_upload_image', [Products::class, 'uploadImage']);
    Route::post('/product_change_image_url', [Products::class, 'changeImageUrl']);

    Route::post('/network_refresh_pairings', [Networks::class, 'refreshPairings']);
    Route::post('/network_modify/{type}', [Networks::class, 'modify']);
});