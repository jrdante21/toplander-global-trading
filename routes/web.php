<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController as Admin;
use App\Http\Controllers\MemberController as Member;
use App\Http\Controllers\SiteController as Site;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::name('site.')->group(function () {
    Route::get('/', [Site::class, 'index'])->name('home');
    Route::get('/about_us', [Site::class, 'aboutUs'])->name('about_us');
    Route::get('/products/{id?}', [Site::class, 'products'])->name('products');
    Route::get('/marketing', [Site::class, 'marketing'])->name('marketing');
    Route::get('/credential/{cred?}', [Site::class, 'credential'])->name('credential');
    Route::get('/register/{any?}', [Site::class, 'register'])->name('register');
});

Route::prefix('distributor')->name('member.')->group(function () {
    Route::get('/', [Member::class, 'loginPage'])->name('login');
    Route::get('/logout', [Member::class, 'logout'])->name('logout');
    Route::post('/login', [Member::class, 'loginForm']);

    Route::middleware('member')->group(function () {
        Route::get('/dashboard/{any?}', [Member::class, 'dashboard'])->where('any', '.*')->name('dashboard');
        Route::get('/genealogy/{any?}', [Member::class, 'genealogy'])->where('any', '.*')->name('genealogy');
        Route::get('/forms/{any?}', [Member::class, 'forms'])->where('any', '.*')->name('forms');
    });
});

Route::prefix('topadminportal')->name('admin.')->group(function () {
    Route::get('/', [Admin::class, 'loginPage'])->name('login');
    Route::get('/logout', [Admin::class, 'logout'])->name('logout');
    Route::post('/login', [Admin::class, 'loginForm']);

    Route::middleware('admin')->group(function () {
        Route::get('/panel/{any?}', [Admin::class, 'panel'])->where('any','.*')->name('panel');
        Route::get('/forms/{any?}', [Admin::class, 'forms'])->where('any','.*')->name('forms');
        Route::get('/forms2/{any?}', [Admin::class, 'forms'])->where('any','.*')->name('forms2');
        Route::get('/genealogy/{any?}', [Admin::class, 'genealogy'])->where('any','.*')->name('genealogy');
    });
});
